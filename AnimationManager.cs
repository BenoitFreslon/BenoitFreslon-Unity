using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class AnimationManager : BFMonoBehaviour
{

	public string CurrentAnimation;
	public float FPS = 0f;
	AnimatedTexture curAnim;
	List<AnimatedTexture> animations = new List<AnimatedTexture> ();

	public delegate void AnimationCompleted (string anim);
	public AnimationCompleted OnComplete;
	// Use this for initialization
	void Start ()
	{
	
	}
	void Awake ()
	{
		foreach (Transform child in transform) {
			
			AnimatedTexture at = child.GetComponent<AnimatedTexture> ();
			if (at) {
				animations.Add (at);
				at.OnComplete += animationCompleted;
				at.Deactive ();
				if (FPS > 0f) {
					at.FPS = FPS;
				}
			}
		}

		// First animation
		if (CurrentAnimation != null) {
			curAnim = GetAnimation (CurrentAnimation);
			CurrentAnimation = curAnim.name;
			curAnim.Active ();
		} else {
			curAnim = animations [0];
			CurrentAnimation = curAnim.name;
			curAnim.Active ();
		}
			
	}
	// Update is called once per frame
	void Update ()
	{

	}
	void animationCompleted ()
	{
		//Debug.Log("animationCompleted" + CurrentAnimation);
		if (OnComplete != null)
			OnComplete (CurrentAnimation);
	}
	public AnimatedTexture GetAnimation (string a)
	{
		if (FindChildByName (a)) {
			if (FindChildByName (a).GetComponent<AnimatedTexture> ()) {
				return FindChildByName (a).GetComponent<AnimatedTexture> ();
			} else {
				Debug.LogError ("No component AnimatedTexture found in animation: " + a + " in " + gameObject);
			}
		} else {
			Debug.LogError ("No animation named " + a + " in " + gameObject);
		}
		return null;
	}
	public void Play (string a)
	{
		//Debug.Log("Play" + a + " curAnim" + curAnim);
		AnimatedTexture oldAnim = curAnim;
		
		if (FindChildByName (a)) {
			curAnim = FindChildByName (a).GetComponent<AnimatedTexture> ();
			curAnim.gameObject.SetActive (true);
			curAnim.Play ();
			CurrentAnimation = a;
		} else {
			Debug.LogError ("Can't find animation: " + a + " " + gameObject);
		}
		if (oldAnim)
			oldAnim.Deactive ();
	}
	public void Pause ()
	{
		if (curAnim != null) {
			curAnim.Pause ();
		}
	}
	public void Resume ()
	{
		if (curAnim != null) {
			curAnim.Resume ();
		}
	}
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceParticleSystem : MonoBehaviour
{
	// Update is called once per frame
	void Update ()
	{
		GetComponent<ParticleSystem> ().Play ();
	}
}

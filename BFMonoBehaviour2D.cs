using UnityEngine;
using System.Collections;

public class BFMonoBehaviour2D : BFMonoBehaviour
{
	public void FadeInOut (float time)
	{
		FadeInOut (time, null);
	}
	public void FadeInOut (float time, System.Action callback)
	{
		gameObject.transform.GetComponent<SpriteRenderer> ().color = new Color (1f, 1f, 1f, 0f);
		LeanTween.alpha (gameObject, 1f, time / 2f).setDestroyOnComplete (true);
		LeanTween.alpha (gameObject, 0f, time / 2f).setDelay (time / 2f).setDestroyOnComplete (true);
		if (callback != null)
			LeanTween.delayedCall (gameObject, time, callback).setDestroyOnComplete (true);
	}
	public void FadeIn (float time)
	{
		FadeIn (time, null);
	}
	public void FadeIn (float time, System.Action callback)
	{
		gameObject.transform.GetComponent<SpriteRenderer> ().color = new Color (1f, 1f, 1f, 0f);
		LeanTween.alpha (gameObject, 1f, time);
		if (callback != null)
			LeanTween.delayedCall (gameObject, time, callback);
	}
	public void FadeOut (float time)
	{
		FadeOut (time, null);
	}
	public void FadeOut (float time, System.Action callback)
	{
		gameObject.transform.GetComponent<SpriteRenderer> ().color = Color.white;
		LeanTween.alpha (gameObject, 0f, time);
		if (callback != null)
			LeanTween.delayedCall (gameObject, time, callback);
	}
	
	// Properties

	
	public float alpha {
		get {
			return GetComponent<SpriteRenderer> ().color.a;
		}
		set {

			foreach (Transform child in transform) {
				SpriteRenderer r = child.gameObject.GetComponent<SpriteRenderer> ();
				if (r) {
					r.color = new Color (r.color.r, r.color.g, r.color.b, value);
				}
			}
			Color c = GetComponent<SpriteRenderer> ().color;
			GetComponent<SpriteRenderer> ().color = new Color (c.r, c.g, c.b, value);
		}
	}
	public TextMesh textMesh {
		get {
			return GetComponent<TextMesh> ();
		}
	}
	public float rotation {
		get {
			return transform.eulerAngles.z;
		}
		set {
			transform.eulerAngles = new Vector3 (transform.rotation.x, transform.rotation.y, value);
		}
	}

	public Color color {
		get {
			return GetComponent<SpriteRenderer> ().color;
		}
		set {
			foreach (Transform child in transform) {
				SpriteRenderer r = child.gameObject.GetComponent<SpriteRenderer> ();
				if (r) {
					r.color = value;
				}
			}
			GetComponent<SpriteRenderer> ().color = value;
		}
	}
	
	public bool visible {
		get {
			return GetComponent<SpriteRenderer> ().enabled;
		}
		set {
			foreach (Transform child in transform) {
				SpriteRenderer r = child.gameObject.GetComponent<SpriteRenderer> ();
				if (r) {
					r.enabled = value;
				}
			}
			GetComponent<SpriteRenderer> ().enabled = value;
		}
	}

}


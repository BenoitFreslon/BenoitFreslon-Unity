﻿using UnityEngine;
using System.Collections;

public class BFTextMesh : MonoBehaviour
{
	public string LayerName = "Default";
	public int OrderInLayer = 0;

	void Awake ()
	{
		if (LayerName != "")
			SetLayerName (LayerName);
		if (OrderInLayer != 0)
			SetOrderInLayer (OrderInLayer);
		SetText (GetComponent<TextMesh> ().text);
		enabled = true;
	}

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	public void SetLayerName (string layerName)
	{
		LayerName = layerName;
		GetComponent<TextMesh> ().GetComponent<Renderer> ().sortingLayerName = LayerName;
	}
	public void SetOrderInLayer (int order)
	{
		OrderInLayer = order;
		GetComponent<TextMesh> ().GetComponent<Renderer> ().sortingOrder = OrderInLayer;
	}
	public void SetText (string text)
	{

		GetComponent<TextMesh> ().text = text.Replace ("\\n", "\n");
	}
}

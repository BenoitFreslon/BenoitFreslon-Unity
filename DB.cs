﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class DB
{
    static public void Log ()
    {
        Debug.Log ( formatMessage ( "" ) );
    }

    static public void LogList ( List<int> list )
    {
        string s = "[";
        for ( int i = 0 ; i < list.Count ; i++ ) {
            if ( i == 0 )
                s += list[ i ];
            else
                s += ", " + list[ i ];
        }
        s += "]";
        Debug.Log ( formatMessage ( s ) );
    }

    static public void LogList ( List<string> list )
    {
        string s = "";
        for ( int i = 0 ; i < list.Count ; i++ ) {
            s += "," + list[ i ] + " ";
        }
        s += "]";
        Debug.Log ( formatMessage ( s ) );
    }

    static public void Log ( object message )
    {
        Debug.Log ( formatMessage ( message ) );
    }

    static public void Log ( params object[] messages )
    {
        string msg = "";
        for ( int i = 0 ; i < messages.Length ; i++ ) {
            if ( messages[ i ] == null )
                messages[ i ] = messages[ i ] = "null";
            msg += messages[ i ].ToString ();
            if ( i != messages.Length - 1 )
                msg += " ";
        }
        Debug.Log ( formatMessage ( msg ) );
    }

    static public void Log ( object message, UnityEngine.Object context )
    {
        Debug.Log ( formatMessage ( message ), context );
    }

    static public void LogWarning ( object message )
    {
        Debug.LogWarning ( formatMessage ( message ) );
    }

    static public void LogWarning ( object message, UnityEngine.Object context )
    {
        Debug.LogWarning ( formatMessage ( message ), context );
    }

    static public void LogError ( object message )
    {
        Debug.LogError ( formatMessage ( message ) );
    }

    static public void LogError ( object message, UnityEngine.Object context )
    {
        Debug.LogError ( formatMessage ( message ), context );
    }

    static private string formatMessage ( object message )
    {

        if ( !BFDevice.IsEditor || BFDevice.IsMobile ) {
            if ( message == null )
                message = "";
            return message.ToString ();
        }

        if ( message == null ) {
            message = "null";
        }
        message = message.ToString ();
        string stack = StackTraceUtility.ExtractStackTrace ().Split ( "\n"[ 0 ] )[ 2 ];
        string obj = stack.Split ( ":"[ 0 ] )[ 0 ];
        string methodName = stack.Split ( ":"[ 0 ] )[ 1 ];
        string line = stack.Split ( ":"[ 0 ] )[ 2 ];
        line = line.Split ( ")"[ 0 ] )[ 0 ];
        methodName = methodName.Split ( "("[ 0 ] )[ 0 ];
        //message = "<color=blue>[" + obj + "][" + methodName + "][" + line + "]</color>\n<color=black><b> " + message + "</b></color>";
        message = "#" + obj + "#<color=blue>[" + obj + "][" + methodName + "][" + line + "]</color> > <color=black><b>" + message + "</b></color>";
        //message = "#" + obj + "#" + message;
        return message + "\n";
    }

    static public void StackTrace ()
    {
        Debug.Log ( StackTraceUtility.ExtractStackTrace () );
    }
}

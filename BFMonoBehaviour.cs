using UnityEngine;
using System.Collections;

public class BFMonoBehaviour : MonoBehaviour
{
    #region Native methods
    // Use this for initialization
    void Start ()
    {

    }

    // Update is called once per frame
    void Update ()
    {

    }
    #endregion

    protected GameObject GO ( string objectName )
    {
        if ( GameObject.Find ( objectName ) ) {
            return GameObject.Find ( objectName ).gameObject;
        } else {
            Debug.LogError ( "No GameObject named *" + objectName + "* found" );
            return null;
        }
    }
    protected GameObject GO ( string objectName, bool activate )
    {
        GameObject go = GO ( objectName );
        go.SetActive ( activate );
        return go;
    }
    protected Vector3 GetPos ( string objectName )
    {
        return GO ( objectName ).transform.position;
    }
    protected void SetLayerName ( GameObject go, string layerName )
    {
        if ( go.GetComponent<SpriteRenderer> () )
            go.GetComponent<SpriteRenderer> ().GetComponent<Renderer> ().sortingLayerName = layerName;
        if ( go.GetComponent<TextMesh> () )
            go.GetComponent<TextMesh> ().GetComponent<Renderer> ().sortingLayerName = layerName;
    }
    protected void SetLayerName ( string layerName )
    {
        SetLayerName ( gameObject, layerName );
    }
    protected void SetLayerName ( string objectName, string layerName )
    {
        SetLayerName ( GO ( objectName ), layerName );
    }
    /*
	protected T GO<T> (string objectName)
	{
		return GO (objectName).GetComponent<T> ();
	}
	protected T GO<T> (string objectName, bool activate)
	{
		return GO (objectName, activate).GetComponent<T> ();
	}
*/
    protected TextMesh TM ( string objectName )
    {
        return GO ( objectName ).GetComponent<TextMesh> ();
    }
    protected BFMonoBehaviour BFGO ( string objectName )
    {
        return GO ( objectName ).GetComponent<BFMonoBehaviour> ();
    }
    /*
	protected T Instanciate<T> (Object original)
	{
		GameObject go = GameObject.Instantiate (original) as GameObject;
		return go.GetComponent<T> ();
	}
	*/
    protected SpriteRenderer spriteRenderer
    {
        get {
            return GetComponent<SpriteRenderer> ();
        }
    }

    public Vector3 GetMousePosition ()
    {
        Vector2 mousePos = Input.mousePosition;
        if ( mousePos.x < 0 || mousePos.x >= Screen.width || mousePos.y < 0 || mousePos.y >= Screen.height )
            return new Vector2 ( 0, 0 );
        Vector3 v = Camera.main.ScreenToWorldPoint ( mousePos );
        v.z = 0;
        return v;
    }
    public Vector2 GetMousePosition2D ()
    {
        Vector2 mousePos = Input.mousePosition;
        if ( mousePos.x < 0 || mousePos.x >= Screen.width || mousePos.y < 0 || mousePos.y >= Screen.height )
            return new Vector2 ( 0, 0 );
        Vector3 v = Camera.main.ScreenToWorldPoint ( mousePos );
        return new Vector2 ( v.x, v.y );
    }
    public void DestroyObjectsWithTag ( string t )
    {
        GameObject[] gos = GameObject.FindGameObjectsWithTag ( t );
        //Debug.Log(gos + " " + gos.Length);
        foreach ( GameObject go in gos )
            DestroyImmediate ( go );
    }
    public GameObject FindChildByName ( string n )
    {
        if ( transform.Find ( n ) ) {
            return transform.Find ( n ).gameObject;
        } else {
            Debug.LogError ( "Can't find child named *" + n + "* in " + name );
            return null;
        }
    }
    public GameObject FindChildByName ( string n, bool activate )
    {
        GameObject go = FindChildByName ( n );
        go.SetActive ( activate );
        return go;
    }
    public Bounds bounds
    {
        get {
            Vector3 center = Vector3.zero;
            foreach ( Transform child in transform ) {
                center += child.gameObject.GetComponent<Renderer> ().bounds.center;
            }
            center /= transform.childCount; //center is average center of children
                                            //Now you have a center, calculate the bounds by creating a zero sized 'Bounds', 
            Bounds b = new Bounds ( center, Vector3.zero );
            foreach ( Transform child in transform ) {
                b.Encapsulate ( child.gameObject.GetComponent<Renderer> ().bounds );
            }
            return b;
        }
    }

    public void Center ()
    {
        transform.position = Vector2.zero;
    }
    public void MoveOut ()
    {
        transform.position = Vector2.one * 100;
    }
    /*
	public bool active {
		get {
			return gameObject.activeSelf;
		}
		set {
			gameObject.SetActive (value);
		}
	}
	*/
    #region Getters Setters
    public float x
    {
        get {
            return transform.position.x;
        }
        set {
            transform.position = new Vector3 ( value, transform.position.y, transform.position.z );
        }
    }
    public float y
    {
        get {
            return transform.position.y;
        }
        set {
            transform.position = new Vector3 ( transform.position.x, value, transform.position.z );
        }
    }
    public float z
    {
        get {
            return transform.position.z;
        }
        set {
            transform.position = new Vector3 ( transform.position.x, transform.position.y, value );
        }
    }
    public float localX
    {
        get {
            return transform.localPosition.x;
        }
        set {
            transform.localPosition = new Vector3 ( value, transform.localPosition.y, transform.localPosition.z );
        }
    }
    public float localY
    {
        get {
            return transform.localPosition.y;
        }
        set {
            transform.localPosition = new Vector3 ( transform.localPosition.x, value, transform.localPosition.z );
        }
    }
    public float localZ
    {
        get {
            return transform.localPosition.z;
        }
        set {
            transform.localPosition = new Vector3 ( transform.localPosition.x, transform.localPosition.y, value );
        }
    }
    public float scaleX
    {
        get {
            return transform.localScale.x;
        }
        set {
            transform.localScale = new Vector3 ( value, transform.localScale.y, transform.localScale.z );
        }
    }
    public float scaleY
    {
        get {
            return transform.localScale.y;
        }
        set {
            transform.localScale = new Vector3 ( transform.localScale.x, value, transform.localScale.z );
        }
    }
    public float scaleZ
    {
        get {
            return transform.localScale.z;
        }
        set {
            transform.localScale = new Vector3 ( transform.localScale.x, transform.localScale.y, value );
        }
    }
    public Vector3 pivot
    {
        get {
            return transform.localPosition;
        }
        set {
            transform.localPosition = value;
        }
    }
    #endregion
}
﻿using UnityEngine;

public class BFColor
{
	public static readonly Color transparent = new Color ( 1, 1, 1, 0 );
	public static readonly Color halfTransparent = new Color ( 1, 1, 1, 0.5f );

	public static Color Alpha ( float a )
	{
		return new Color ( 1, 1, 1, a );
	}

	public static Color Alpha ( Color c, float a )
	{
		c.a = a;
		return c;
	}
}

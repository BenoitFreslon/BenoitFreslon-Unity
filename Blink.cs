﻿using UnityEngine;
using System.Collections;

public class Blink : MonoBehaviour
{
    //public bool UsePositionX = false;
    //public bool UsePositionY = false;
    //public bool UseScale = false;


    float curTime = 0;
    float interval;
    int numTotalBlinks;
    int numBlinks = 0;
    Color oldColor;
    Vector3 oldPos;
    Vector3 oldScale;
    bool disabled = false;
    bool firstPlay = true;


    [System.Serializable]
    public enum BlinkType
    {
        Scale,
        PositionX,
        PositionY,
        Renderer,
        Active
    }

    public BlinkType Type;

    void Start ()
    {
    }

    void Update ()
    {
        if ( numTotalBlinks != 0 ) {
            curTime += Time.deltaTime;
            if ( curTime >= interval ) {
                curTime -= interval;
                if ( !disabled ) {
                    if ( Type == BlinkType.Scale )
                        transform.localScale = Vector3.zero;
                    else if ( Type == BlinkType.PositionX )
                        transform.Translate ( new Vector3 ( -1000, 0, 0 ) );
                    else if ( Type == BlinkType.PositionY )
                        transform.Translate ( new Vector3 ( 0, -1000, 0 ) );
                    else if ( Type == BlinkType.Renderer )
                        GetComponent<Renderer> ().material.color = Color.clear;
                    else if ( Type == BlinkType.Active )
                        gameObject.SetActive ( false );
                    disabled = true;
                } else {
                    numBlinks++;
                    if ( Type == BlinkType.Scale )
                        transform.localScale = oldScale;
                    else if ( Type == BlinkType.PositionX )
                        transform.position = new Vector3 ( oldPos.x, transform.position.y, transform.position.z );
                    else if ( Type == BlinkType.PositionY )
                        transform.position = new Vector3 ( transform.position.x, oldPos.y, transform.position.z );
                    else if ( Type == BlinkType.Renderer )
                        GetComponent<Renderer> ().material.color = oldColor;
                    else if ( Type == BlinkType.Active )
                        gameObject.SetActive ( true );

                    disabled = false;
                }

                if ( numBlinks == numTotalBlinks ) {
                    numTotalBlinks = 0;
                }
            }
        }
    }

    public void Init ()
    {

        //		if ( UseScale )
        //			Type = BlinkType.Scale;
        //		if ( UsePositionX )
        //			Type = BlinkType.PositionX;
        //		if ( UsePositionY )
        //			Type = BlinkType.PositionY;

        if ( Type == BlinkType.Scale )
            oldScale = transform.localScale;
        else if ( Type == BlinkType.PositionX || Type == BlinkType.PositionY )
            oldPos = transform.position;
        else if ( Type == BlinkType.Renderer )
            oldColor = GetComponent<Renderer> ().material.color;
        else if ( Type == BlinkType.Active )
            gameObject.SetActive ( true );
    }

    public void Play ( float time = 1, int num = 3 )
    {
        Stop ();
        numTotalBlinks = num;
        numBlinks = 0;
        curTime = 0;
        interval = time / ( float )num;
        interval /= 2;
        if ( firstPlay ) {
            Init ();
        }
        firstPlay = false;
    }

    public void Stop ()
    {
        Debug.Log ( "Stop" );
        if ( !firstPlay ) {
            if ( Type == BlinkType.PositionX || Type == BlinkType.PositionY )
                transform.position = oldPos;
            else if ( Type == BlinkType.Scale )
                transform.localScale = oldScale;
            else if ( Type == BlinkType.Renderer )
                GetComponent<Renderer> ().material.color = oldColor;
            else if ( Type == BlinkType.Active )
                gameObject.SetActive ( true );
        }

        numTotalBlinks = 0;


    }
}

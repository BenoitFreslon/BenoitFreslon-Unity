﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MobileMarkets
{
	// https://gist.github.com/daFish/5990634


	public static string AppStoreId = "";
	public static string GooglePlayId = "";
	public static string WebUrl = "";

	public static string AppStoreUrl = "https://itunes.apple.com/[LANG]/app/id[APPID]?mt=8";
	public static string GooglePlayUrl = "https://play.google.com/store/apps/details?id=[APPID]&hl=[LANG]";

	static private Dictionary<string,string> languages = new Dictionary<string,string> () {
		{ "Afrikaans",		"af" },
		{ "Arabic",			"ar" },
		{ "Basque",			"ue" },
		{ "Belarusian",		"by" },
		{ "Bulgarian",		"bg" },
		{ "Catalan",		"es" },
		{ "Chinese",		"cn" },
		{ "Czech",			"cz" },
		{ "Danish",			"dk" },
		{ "Dutch",			"nl" },
		{ "English",		"en" },
		{ "Estonian",		"ee" },
		{ "Faroese",		"dk" },
		{ "Finnish",		"fi" },
		{ "French",			"fr" },
		{ "German",			"de" },
		{ "Greek",			"gr" },
		{ "Hebrew",			"il" },
		{ "Icelandic",		"is" },
		{ "Indonesian",		"id" },
		{ "Italian",		"it" },
		{ "Japanese",		"jp" },
		{ "Korean",			"kr" },
		{ "Latvian",		"lv" },
		{ "Lithuanian",		"lt" },
		{ "Norwegian",		"no" },
		{ "Polish",			"pl" },
		{ "Portuguese",		"pt" },
		{ "Romanian",		"ro" },
		{ "Russian",		"ru" },
		{ "SerboCroatian",	"hr" },
		{ "Slovak",			"sk" },
		{ "Slovenian",		"si" },
		{ "Spanish",		"es" },
		{ "Swedish",		"se" },
		{ "Thai",			"th" },
		{ "Turkish",		"tr" },
		{ "Ukrainian",		"ua" },
		{ "Vietnamese",		"vn" },
		{ "ChineseSimplified",	"cn" },
		{ "ChineseTraditional",	"cn" },
		{ "Unknown",		"" },
		{ "Hungarian",		"hu" }
	};

	public static string GetLocalizedUrl ()
	{
		if (languages.ContainsKey (Application.systemLanguage.ToString ())) {
			return ParseUrl (Application.systemLanguage.ToString ());
		} 
		return ParseUrl ("en");
	}

	private static string ParseUrl (string lang)
	{

		if (Application.platform == RuntimePlatform.Android || true) {
			return GooglePlayUrl.Replace ("[LANG]", lang).Replace ("[APPID]", GooglePlayId);
		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
			return AppStoreUrl.Replace ("[LANG]", lang).Replace ("[APPID]", AppStoreId);
		} else {
			return WebUrl;
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PoolGameObject
{
	protected int curId = 0;
	protected List<GameObject> pool = new List<GameObject> ();
	protected Transform parent;
	protected GameObject prefab;

	public void Init (GameObject go, Transform par, int num = 100)
	{
		prefab = go;
		parent = par;
		for (int i = 0; i < num; i++) {
			Add ();
		}
	}

	public GameObject Get ()
	{
		if (curId > pool.Count - 1) {
			Debug.Log ("add go");
			Add ();
		}
		curId++;
		pool [curId].SetActive (true);
		return pool [curId];
	}

	public void Add ()
	{
		GameObject go = GameObject.Instantiate (prefab);
		go.transform.parent = parent;
		go.SetActive (false);
		pool.Add (go);
	}

	public void Remove (GameObject go)
	{
		go.SetActive (false);
		curId--;
	}

	public void RemoveAll ()
	{
		foreach (GameObject go in pool)
			go.SetActive (false);
		curId = 0;
	}
}

using UnityEngine;
using System.Collections;

/// <summary>
/// Get device informations
/// </summary>
public class BFDevice
{
    /// <summary>
    /// Check if the platform is a tablet
    /// </summary>
    /// <returns><c>true</c> if is tablet; otherwise, <c>false</c>.</returns>
    static public bool IsTablet
    {
        get {
            if ( Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer ) {
                float screenWidth = Screen.width / Screen.dpi;
                float screenHeight = Screen.height / Screen.dpi;
                double size = Mathf.Sqrt ( Mathf.Pow ( screenWidth, 2 ) + Mathf.Pow ( screenHeight, 2 ) );
                if ( size >= 6 )
                    return true;
            }
            return false;
        }
    }

    static public bool IsMobile
    {
        get {
            if ( Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer ) {
                return true;
            }
            return false;
        }
    }

    static public bool IsEditor
    {
        get {
            if ( Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor ) {
                return true;
            }
            return false;
        }
    }

    static public bool IsWebPlayer
    {
        get {
            if ( Application.platform == RuntimePlatform.WebGLPlayer ) {
                return true;
            }
            return false;
        }
    }

    static public bool IsPlayer
    {
        get {
            if ( Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.LinuxPlayer ) {
                return true;
            }
            return false;
        }
    }

    static public float ScaleFactor
    {
        get {
            if ( Screen.width <= 480 ) {
                return 1f;
            } else if ( Screen.width > 480 ) {
                return 2f;
            } else if ( Screen.width > 1136 ) {
                return 4f;
            }
            return 1f;
        }
    }

    static public bool IsAndroid
    {
        get {
            if ( Application.platform == RuntimePlatform.Android ) {
                return true;
            }
            return false;
        }
    }

    static public bool IsIPhone
    {
        get {

            if ( Application.platform == RuntimePlatform.IPhonePlayer ) {
                return true;
            }
            return false;
        }
    }

    public static bool IsAndroidBuild
    {
        get {
#if UNITY_ANDROID
            return true;
#else
            return false;
#endif
        }
    }

    public static bool IsIPhoneBuild
    {
        get {
#if UNITY_IPHONE
            return true;
#else
            return false;
#endif
        }
    }

    public static bool IsMobileBuild
    {
        get {
#if UNITY_IPHONE || UNITY_IPHONE
            return true;
#else
            return false;
#endif
        }
    }

    /** See https://developer.android.com/guide/topics/manifest/uses-sdk-element.html **/
    static public int AndroidVersion
    {
        get {
            int iVersionNumber = 0;
            if ( Application.platform == RuntimePlatform.Android ) {
                string androidVersion = SystemInfo.operatingSystem;
                int sdkPos = androidVersion.IndexOf ( "API-" );
                iVersionNumber = int.Parse ( androidVersion.Substring ( sdkPos + 4, 2 ).ToString () );
            }
            return iVersionNumber;
        }
    }

    public static float iOSVersion
    {
        get {
            // SystemInfo.operatingSystem returns something like iPhone OS 6.1
            float osVersion = 1f;
#if UNITY_IOS
            string versionString = UnityEngine.iOS.Device.systemVersion;
            float.TryParse ( versionString, out osVersion );
#endif
            return osVersion;
        }
    }

    static public void FitToScreenWidth ( float originalSize, float ratio, int width, int height )
    {
        //Debug.Log("originalSize" + originalSize + " ratio: " + ratio + " " + width + " " + height);
        float coef = originalSize * ratio;
        Camera.main.GetComponent<Camera> ().orthographicSize = ( float )( coef / ( ( float )width / ( float )height ) );
    }

    static public void FitToScreenHeight ( float originalSize, float ratio, int width, int height )
    {
        float coef = originalSize * ratio;
        Camera.main.GetComponent<Camera> ().orthographicSize = ( float )( coef / ( ( float )height / ( float )width ) );
    }

    public static void OpenURL ( string url )
    {
        Application.OpenURL ( url );
        /*
		if (IsWebPlayer)
			Application.ExternalEval ("window.open('" + url + "','_blank')");
		else
			Application.OpenURL (url);
		*/
    }
}
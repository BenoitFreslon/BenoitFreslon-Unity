using UnityEngine;
using System.Collections;

public class Sitelock
{

    static public bool allowDomains(string[] domains, bool allowLocal = true)
    {
        if (BFDevice.IsWebPlayer)
        {
            string url = Application.absoluteURL;
            foreach (string domain in domains)
            {
                Debug.Log("Sitelock: " + url + " " + domain);
                if (url.Contains("file:///"))
                {
                    return true;
                }
                if (url.Contains(domain))
                {
                    return true;
                }

            }
            Camera.main.transform.position = new Vector3(-100, -100, -10);
            GameObject go = new GameObject("~Sitelock");

            go.AddComponent<TextMesh>();
            go.transform.position = new Vector3(0, 0, 0);
            go.GetComponent<TextMesh>().text = "Sorry, this game is sitelocked.\nContact me if you want this game on your website\n\ncontact@benoitfreslon.com";

            return false;
        }
        else
        {
            Debug.Log("Sitelock: It's not a web application");
            return true;
        }
    }
}


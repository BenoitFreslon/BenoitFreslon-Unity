using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class BFUtils
{
#if UNITY_EDITOR
    public static EditorWindow GetMainGameView ()
    {
        System.Type T = System.Type.GetType ( "UnityEditor.GameView,UnityEditor" );
        System.Reflection.MethodInfo GetMainGameView = T.GetMethod ( "GetMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static );
        System.Object Res = GetMainGameView.Invoke ( null, null );
        return (EditorWindow)Res;
    }
#endif
    public static Sprite LoadSpriteFromResources ( string path )
    {
        Sprite s = Resources.Load ( path, typeof ( Sprite ) ) as Sprite;
        if ( s == null )
            Debug.LogError ( "Can't load Sprite " + path );
        return s;
    }

    public static bool IsFileExists ( string path )
    {
        UnityEngine.Object o = Resources.Load ( path );
        if ( o == null )
            return false;
        else
            return true;
    }

    public static void RemoveAllChildren ( GameObject go )
    {
        while ( go.transform.childCount > 0 )
            GameObject.DestroyImmediate ( go.transform.GetChild ( 0 ).gameObject );
    }

    public static string GetCharAt ( string s, int i )
    {
        if ( s.Length - 1 < i ) {
            return "";
        }
        return s.ToCharArray ()[ i ].ToString ();
    }

    public static int GetIntAt ( int num, int i )
    {
        string c = GetCharAt ( num.ToString (), i );
        if ( c == "" )
            return 0;
        else
            return int.Parse ( c );
    }

    public static void SetColor ( GameObject go, Color col )
    {
        if ( go.GetComponent<SpriteRenderer> () )
            go.GetComponent<SpriteRenderer> ().color = col;
        foreach ( Transform child in go.transform ) {
            if ( child.GetComponent<SpriteRenderer> () )
                child.GetComponent<SpriteRenderer> ().color = col;
        }
    }

    public static GameObject LoadGameObjectFromResources ( string path )
    {
        GameObject go = GameObject.Instantiate ( Resources.Load ( path, typeof ( GameObject ) ) as GameObject );
        if ( go == null )
            Debug.LogError ( "Can't load GameObject " + path );
        return go;
    }

    #region Canvas

    /// <summary>
    /// Calulates Position for RectTransform.position from a transform.position. Does not Work with WorldSpace Canvas!
    /// </summary>
    /// <param name="_Canvas"> The Canvas parent of the RectTransform.</param>
    /// <param name="_Position">Position of in world space of the "Transform" you want the "RectTransform" to be.</param>
    /// <param name="_Cam">The Camera which is used. Note this is useful for split screen and both RenderModes of the Canvas.</param>
    /// <returns></returns>
    public static Vector3 CalculatePositionFromTransformToRectTransform ( Canvas _Canvas, Vector3 _Position, Camera _Cam )
    {
        Vector3 Return = Vector3.zero;
        if ( _Canvas.renderMode == RenderMode.ScreenSpaceOverlay ) {
            Return = _Cam.WorldToScreenPoint ( _Position );
        } else if ( _Canvas.renderMode == RenderMode.ScreenSpaceCamera ) {
            Vector2 tempVector = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle ( _Canvas.transform as RectTransform, _Cam.WorldToScreenPoint ( _Position ), _Cam, out tempVector );
            Return = _Canvas.transform.TransformPoint ( tempVector );
        }

        return Return;
    }

    /// <summary>
    /// Calulates Position for RectTransform.position Mouse Position. Does not Work with WorldSpace Canvas!
    /// </summary>
    /// <param name="_Canvas">The Canvas parent of the RectTransform.</param>
    /// <param name="_Cam">The Camera which is used. Note this is useful for split screen and both RenderModes of the Canvas.</param>
    /// <returns></returns>
    public static Vector3 CalculatePositionFromMouseToRectTransform ( Canvas _Canvas, Camera _Cam )
    {
        Vector3 Return = Vector3.zero;

        if ( _Canvas.renderMode == RenderMode.ScreenSpaceOverlay ) {
            Return = Input.mousePosition;
        } else if ( _Canvas.renderMode == RenderMode.ScreenSpaceCamera ) {
            Vector2 tempVector = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle ( _Canvas.transform as RectTransform, Input.mousePosition, _Cam, out tempVector );
            Return = _Canvas.transform.TransformPoint ( tempVector );
        }

        return Return;
    }

    /// <summary>
    /// Calculates Position for "Transform".position from a "RectTransform".position. Does not Work with WorldSpace Canvas!
    /// </summary>
    /// <param name="_Canvas">The Canvas parent of the RectTransform.</param>
    /// <param name="_Position">Position of the "RectTransform" UI element you want the "Transform" object to be placed to.</param>
    /// <param name="_Cam">The Camera which is used. Note this is useful for split screen and both RenderModes of the Canvas.</param>
    /// <returns></returns>
    public static Vector3 CalculatePositionFromRectTransformToTransform ( Canvas _Canvas, Vector3 _Position, Camera _Cam )
    {
        Vector3 Return = Vector3.zero;
        if ( _Canvas.renderMode == RenderMode.ScreenSpaceOverlay ) {
            Return = _Cam.ScreenToWorldPoint ( _Position );
        } else if ( _Canvas.renderMode == RenderMode.ScreenSpaceCamera ) {
            RectTransformUtility.ScreenPointToWorldPointInRectangle ( _Canvas.transform as RectTransform, _Cam.WorldToScreenPoint ( _Position ), _Cam, out Return );
        }
        return Return;
    }

    static public Rect GetWorldRect ( RectTransform rt, Vector2 scale )
    {
        // Convert the rectangle to world corners and grab the top left
        Vector3[] corners = new Vector3[ 4 ];
        rt.GetWorldCorners ( corners );
        Vector3 topLeft = corners[ 0 ];

        // Rescale the size appropriately based on the current Canvas scale
        Vector2 scaledSize = new Vector2 ( scale.x * rt.rect.size.x, scale.y * rt.rect.size.y );

        return new Rect ( topLeft, scaledSize );
    }

    #endregion

    public static XmlDocument LoadXMLFromResources ( string path )
    {

        TextAsset textAsset = (TextAsset)Resources.Load ( path );
        if ( textAsset == null )
            Debug.LogError ( "Can't load XmlDocument " + path );
        XmlDocument asset = new XmlDocument ();
        asset.LoadXml ( textAsset.text );
        return asset;
    }

    public static string[] ShuffleStrings ( string[] arr )
    {
        List<string> list = new List<string> ();

        foreach ( string s in arr ) {
            list.Add ( s );
        }

        list = ShuffleList ( list );

        string[] result = new string[ arr.Length ];
        int index = 0;
        foreach ( string s in list ) {
            result[ index ] = s;
            index++;
        }
        return result;
    }

    public static List<T> ShuffleList<T> ( List<T> list )
    {
        List<T> randomizedList = new List<T> ();
        System.Random rnd = new System.Random ();
        while ( list.Count > 0 ) {
            int index = rnd.Next ( 0, list.Count ); //pick a random item from the master list
            randomizedList.Add ( list[ index ] ); //place it at the end of the randomized list
            list.RemoveAt ( index );
        }
        return randomizedList;
    }

    static public string ScoreToString ( int value )
    {
        return value.ToString ( "n0" );
        /*
		// Debug.LogWarning (score);
		if (value >= 1000) {
			return value.ToString ("##0");
		} else if (value >= 1000000) {
			return value.ToString ("### ##0");
		} else if (value >= 1000000000) {
			return value.ToString ("### ### ##0");
		} else {
			return value.ToString ();
		}
		*/
    }

    static public string TimeToString ( int time )
    {
        int min = (int)( (float)time / 60f );
        int sec = (int)( (float)time % 60f );
        string secString = sec.ToString ();
        if ( sec < 10 ) {
            secString = "0" + sec.ToString ();
        }
        string minString = min.ToString ();
        if ( min < 10 ) {
            minString = "0" + min.ToString ();
        }
        return minString + ":" + secString;
    }
    // Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
    static public string ColorToHex ( Color32 color )
    {
        string hex = color.r.ToString ( "X2" ) + color.g.ToString ( "X2" ) + color.b.ToString ( "X2" );
        return "#" + hex.ToLower ();
    }

    static public Color HexToColor ( string hex )
    {
        byte r = byte.Parse ( hex.Substring ( 0, 2 ), System.Globalization.NumberStyles.HexNumber );
        byte g = byte.Parse ( hex.Substring ( 2, 2 ), System.Globalization.NumberStyles.HexNumber );
        byte b = byte.Parse ( hex.Substring ( 4, 2 ), System.Globalization.NumberStyles.HexNumber );
        return new Color32 ( r, g, b, 255 );
    }

    static public float GetHue ( Color col )
    {
        return GetHue ( col.r, col.g, col.b );


    }

    static public Color Color255 ( float r, float g, float b )
    {
        return new Color ( r / 255f, g / 255f, b / 255f );
    }

    static public float GetHue ( float red, float green, float blue )
    {
        Debug.Log ( red + " " + green + " " + blue );
        float min = Mathf.Min ( Mathf.Min ( red, green ), blue );
        float max = Mathf.Max ( Mathf.Max ( red, green ), blue );

        float hue = 0f;
        if ( max == red ) {
            hue = ( green - blue ) / ( max - min );
        } else if ( max == green ) {
            hue = 2f + ( blue - red ) / ( max - min );
        } else {
            hue = 4f + ( red - green ) / ( max - min );
        }

        hue = hue * 60f;
        if ( hue < 0f )
            hue = hue + 360f;

        return hue;
    }

    static public AnimationClip GetCurrentAnimation ( GameObject go )
    {
        Animator anim = go.GetComponent<Animator> ();
        RuntimeAnimatorController ac = anim.runtimeAnimatorController;
        for ( int i = 0; i < ac.animationClips.Length; i++ ) {                 //For all animations
            if ( anim.GetCurrentAnimatorStateInfo ( 0 ).IsName ( ac.animationClips[ i ].name ) ) {        //If it has the same name as your clip
                return ac.animationClips[ i ];
            }
        }
        return null;
    }

    static public float GetTimeCurrentAnimation ( GameObject go )
    {
        Animator anim = go.GetComponent<Animator> ();
        RuntimeAnimatorController ac = anim.runtimeAnimatorController;
        for ( int i = 0; i < ac.animationClips.Length; i++ ) {                 //For all animations
            if ( anim.GetCurrentAnimatorStateInfo ( 0 ).IsName ( ac.animationClips[ i ].name ) ) {        //If it has the same name as your clip
                return ac.animationClips[ i ].length;
            }
        }
        return 0;
    }

    static public string RemoveBetween ( string s, string begin, string end )
    {
        string[] sep1 = new string[] { begin };
        string[] sep2 = new string[] { end };
        string[] arr1 = s.Split ( sep1, StringSplitOptions.None );
        string final = arr1[ 0 ];
        if ( arr1.Count () > 1 ) {
            string s2 = arr1[ 1 ];
            string[] arr2 = s2.Split ( sep2, StringSplitOptions.None );
            //Debug.Log ("add more string", final, arr [1]);
            if ( arr2.Count () > 1 )
                final += arr2[ 1 ];
        }
        return final;
    }

    static public Color32 AverageColorFromTexture ( Texture2D tex )
    {

        Color32[] texColors = tex.GetPixels32 ();

        int total = texColors.Length;

        float r = 0;
        float g = 0;
        float b = 0;

        for ( int i = 0; i < total; i++ ) {

            r += texColors[ i ].r;

            g += texColors[ i ].g;

            b += texColors[ i ].b;

        }

        return new Color32 ( (byte)( r / total ), (byte)( g / total ), (byte)( b / total ), 0 );

    }

    static public string SafeFilePath ( string enter )
    {
        string exit = enter;

        string[] specialChars = new string[] {
            "Ž",
            "ž",
            "Ÿ",
            "¡",
            "¢",
            "£",
            "¤",
            "¥",
            "¦",
            "§",
            "¨",
            "©",
            "ª",
            "À",
            "Á",
            "Â",
            "Ã",
            "Ä",
            "Å",
            "Æ",
            "Ç",
            "È",
            "É",
            "Ê",
            "Ë",
            "Ì",
            "Í",
            "Î",
            "Ï",
            "Ð",
            "Ñ",
            "Ò",
            "Ó",
            "Ô",
            "Õ",
            "Ö",
            "Ù",
            "Ú",
            "Û",
            "Ü",
            "Ý",
            "Þ",
            "ß",
            "à",
            "á",
            "â",
            "ã",
            "ä",
            "å",
            "æ",
            "ç",
            "è",
            "é",
            "ê",
            "ë",
            "ì",
            "í",
            "î",
            "ï",
            "ð",
            "ñ",
            "ò",
            "ó",
            "ô",
            "õ",
            "ö",
            "ù",
            "ú",
            "û",
            "ü",
            "ý",
            "þ",
            "ÿ"
        };
        string[] convertedChars = new string[] {
            "%8E",
            "%9E",
            "%9F",
            "%A1",
            "%A2",
            "%A3",
            "%A4",
            "%A5",
            "%A6",
            "%A7",
            "%A8",
            "%A9",
            "%AA",
            "%C0",
            "%C1",
            "%C2",
            "%C3",
            "%C4",
            "%C5",
            "%C6",
            "%C7",
            "%C8",
            "%C9",
            "%CA",
            "%CB",
            "%CC",
            "%CD",
            "%CE",
            "%CF",
            "%D0",
            "%D1",
            "%D2",
            "%D3",
            "%D4",
            "%D5",
            "%D6",
            "%D9",
            "%DA",
            "%DB",
            "%DC",
            "%DD",
            "%DE",
            "%DF",
            "%E0",
            "%E1",
            "%E2",
            "%E3",
            "%E4",
            "%E5",
            "%E6",
            "%E7",
            "%E8",
            "%E9",
            "%EA",
            "%EB",
            "%EC",
            "%ED",
            "%EE",
            "%EF",
            "%F0",
            "%F1",
            "%F2",
            "%F3",
            "%F4",
            "%F5",
            "%F6",
            "%F9",
            "%FA",
            "%FB",
            "%FC",
            "%FD",
            "%FE",
            "%FF"
        };

        for ( var i = 0; i < specialChars.Length; i++ ) {
            exit = exit.Replace ( specialChars[ i ], convertedChars[ i ] );
        }

        return exit;
    }

    public static string TruncateAtWord ( string value, int length )
    {
        if ( value == null || value.Length < length || value.IndexOf ( " ", length ) == -1 )
            return value;

        return value.Substring ( 0, value.IndexOf ( " ", length ) );
    }

    public static string RemoveAccents ( string source )
    {
        string decomposed = source.Normalize ( NormalizationForm.FormD );
        char[] filtered = decomposed
            .Where ( c => char.GetUnicodeCategory ( c ) != UnicodeCategory.NonSpacingMark )
            .ToArray ();
        return new String ( filtered );
    }



}


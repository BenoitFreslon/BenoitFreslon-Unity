﻿using UnityEngine;
using System.Collections;

public class ScreenCoordinates : MonoBehaviour
{
	public Vector2 Position;
	// Use this for initialization
	void Awake ()
	{
		//SetPosition (Position);
	}

	void Start ()
	{
		SetPosition (Position);
	}

	public void SetPosition (Vector2 pos)
	{
		Position = pos;
		float z = transform.position.z;
		transform.position = Camera.main.ViewportToWorldPoint (Position);
		transform.position = new Vector3 (transform.position.x, transform.position.y, z);
	}

	public void UpdatePosition ()
	{
		SetPosition (Position);
	}
}
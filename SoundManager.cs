using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// SoundManager class
/// </summary>
public class SoundManager : MonoBehaviour
{

    public static bool IsDebug = false;

    public bool BGMMuted = false;

    [Header ( "Add Sounds here" )]
    public AudioClip[] Sounds;
    public AudioClip[] BGM;

    [Header ( "Don't add sounds here" )]
    public Dictionary<string, AudioSource> AllSoundsDict = new Dictionary<string, AudioSource> ();
    public Dictionary<string, AudioSource> SFXDict = new Dictionary<string, AudioSource> ();
    public Dictionary<string, AudioSource> BGMDict = new Dictionary<string, AudioSource> ();

    protected static SoundManager instance;


    protected List<AudioSource> allPlayingSounds = new List<AudioSource> ();

    protected float volumeBGM = 1f;
    protected float volumeSFX = 1f;

    public static SoundManager GetInstance ()
    {
        if ( instance != null ) {
            return instance;
        } else if ( GameObject.Find ( "~SoundManager" ) && GameObject.Find ( "~SoundManager" ).GetComponent<SoundManager> () ) {
            return GameObject.Find ( "~SoundManager" ).GetComponent<SoundManager> ();
        } else if ( instance == null ) {
            //			Debug.Log ("Create SoundManager instance");
            GameObject go = new GameObject ( "~SoundManager" );
            go.AddComponent ( typeof ( SoundManager ) );


        }
        return instance;
    }

    #region Native methods

    void Awake ()
    {
        instance = this;
        if ( Sounds != null ) {
            foreach ( AudioClip audioClip in Sounds ) {
                if ( audioClip != null )
                    AddSFX ( audioClip );
            }

        }
        if ( BGM != null ) {
            foreach ( AudioClip audioClip in BGM ) {
                if ( audioClip != null )
                    AddBGM ( audioClip );
            }
        }
        if ( PlayerPrefs.GetInt ( "BGMMuted", 0 ) == 1 )
            MuteBGM ();
    }

    void Start ()
    {
        transform.position = new Vector3 ( -10f, 0f, 0f );
    }

    void OnDestroy ()
    {
        instance = null;
    }

    #endregion

    #region IsExist

    public bool IsSoundExists ( string soundName )
    {
        if ( AllSoundsDict.ContainsKey ( soundName ) ) {
            return true;
        } else {
            return false;
        }
    }

    #endregion

    #region Add / Remove

    public AudioSource AddSFX ( AudioClip audioClip )
    {
        return AddSFX ( audioClip, audioClip.name );
    }

    public AudioSource AddSFX ( AudioClip audioClip, string audioClipName )
    {
        if ( IsDebug )
            Debug.Log ( "SoundManager AddSound: " + audioClip.name );

        GameObject go = new GameObject ( "GameObject" );

        go.name = audioClipName;
        go.transform.parent = transform;
        go.AddComponent<AudioSource> ();
        go.GetComponent<AudioSource> ().clip = audioClip;
        go.GetComponent<AudioSource> ().playOnAwake = false;
        go.transform.localPosition = Vector3.zero;

        if ( go.tag == "Player" )
            BGMDict.Add ( audioClipName, go.GetComponent<AudioSource> () );
        else
            SFXDict.Add ( audioClipName, go.GetComponent<AudioSource> () );

        AllSoundsDict.Add ( audioClipName, go.GetComponent<AudioSource> () );

        return go.GetComponent<AudioSource> ();
    }

    public AudioSource AddSFX ( string path )
    {
        AudioClip a = Resources.Load ( path ) as AudioClip;
        if ( a == null ) {
            Debug.LogWarning ( "[SoundManager] AddSound: can't load sound in the Resources folder " + path );
        }
        return AddSFX ( a );
    }

    public void AddBGM ( AudioClip audioClip )
    {
        AudioSource a = AddSFX ( audioClip );
        if ( BGMMuted )
            a.volume = 0;
        a.tag = "Player";
    }

    public void AddBGM ( string path )
    {
        AudioSource a = AddSFX ( path );
        if ( BGMMuted )
            a.volume = 0;
        a.tag = "Player";
    }

    public void RemoveSound ( string soundName )
    {
        if ( AllSoundsDict.ContainsKey ( soundName ) ) {
            GameObject.Destroy ( AllSoundsDict[ soundName ].gameObject );
            BGMDict.Remove ( soundName );
            SFXDict.Remove ( soundName );
            AllSoundsDict.Remove ( soundName );
        }
    }

    public void RemoveSound ( AudioClip audioClip )
    {
        RemoveSound ( audioClip.name );
    }

    #endregion

    #region Get sound

    public AudioSource GetSound ( string soundName )
    {
        if ( IsDebug )
            Debug.Log ( "[SoundManager] GetSound: " + soundName + "" );
        if ( AllSoundsDict.ContainsKey ( soundName ) ) {
            if ( BGMDict.ContainsKey ( soundName ) && PlayerPrefs.GetInt ( "BGMMuted", 0 ) == 1 ) {
                BGMDict[ soundName ].volume = 0;
            }
            return AllSoundsDict[ soundName ];
        }
        Debug.LogWarning ( "[SoundManager] GetSound can't found sound: " + soundName );
        return null;
    }

    public AudioSource GetSound ( AudioClip audioClip )
    {
        if ( AllSoundsDict.ContainsKey ( audioClip.name ) ) {
            if ( BGMDict.ContainsKey ( audioClip.name ) && PlayerPrefs.GetInt ( "BGMMuted", 0 ) == 1 ) {
                BGMDict[ audioClip.name ].volume = 0;
            }
            return AllSoundsDict[ audioClip.name ];
        }
        Debug.LogWarning ( "[SoundManager] GetSound can't found sound: " + audioClip.name );
        return null;
    }

    #endregion

    #region Pause / Resume

    public void PauseAllSounds ()
    {
        allPlayingSounds.Clear ();
        foreach ( AudioSource audioSource in AllSoundsDict.Values ) {
            if ( audioSource.isPlaying ) {
                audioSource.Pause ();
                allPlayingSounds.Add ( audioSource );
            }
        }
    }

    public void ResumeAllSounds ()
    {
        allPlayingSounds.Clear ();
        foreach ( AudioSource sound in allPlayingSounds ) {
            sound.UnPause ();
        }
        allPlayingSounds.Clear ();
    }

    public void StopAllSounds ()
    {
        if ( IsDebug )
            Debug.Log ( "[SoundManager] StopAllSounds" );
        foreach ( AudioSource audioSource in SFXDict.Values ) {
            audioSource.Stop ();
        }
    }

    #endregion


    public void StopAllBGM ()
    {
        if ( IsDebug )
            Debug.Log ( "[SoundManager] StopAllBGM" );
        foreach ( AudioSource audioSource in BGMDict.Values ) {
            if ( audioSource.tag == "Player" ) {
                audioSource.GetComponent<AudioSource> ().Stop ();
            }
        }
    }

    #region Mute / Umute

    public void MuteBGM ()
    {
        BGMMuted = true;
        foreach ( AudioSource audioSource in BGMDict.Values ) {
            if ( IsDebug )
                Debug.Log ( "[SoundManager] Mute sound: " + audioSource.name );
            audioSource.volume = 0;
        }
        PlayerPrefs.SetInt ( "BGMMuted", 1 );
    }

    public void UnmuteBGM ()
    {
        BGMMuted = false;
        if ( IsDebug )
            Debug.Log ( "[SoundManager] UnmteBGM" );
        foreach ( AudioSource audioSource in BGMDict.Values ) {
            audioSource.volume = VolumeBGM;
        }
        PlayerPrefs.SetInt ( "BGMMuted", 0 );
    }

    public void MuteSFX ()
    {
        BGMMuted = true;
        foreach ( AudioSource audioSource in BGMDict.Values ) {
            if ( IsDebug )
                Debug.Log ( "[SoundManager] Mute sfx: " + audioSource.name );
            audioSource.volume = 0;
        }
        PlayerPrefs.SetInt ( "SFXMuted", 1 );
    }

    public void UnmuteSFX ()
    {
        BGMMuted = false;
        if ( IsDebug )
            Debug.Log ( "[SoundManager] UnmteBGM sfx" );
        foreach ( AudioSource audioSource in BGMDict.Values ) {
            audioSource.volume = VolumeSFX;
        }
        PlayerPrefs.SetInt ( "SFXMuted", 0 );
    }

    #endregion

    #region Volume

    public float VolumeBGM {
        set {
            volumeBGM = value;
            if ( volumeBGM == 0f )
                MuteBGM ();
            else
                UnmuteBGM ();
        }
        get {
            return volumeBGM;
        }
    }

    public float VolumeSFX {
        set {
            volumeSFX = value;
            if ( volumeSFX == 0f )
                MuteSFX ();
            else
                UnmuteSFX ();
        }
        get {
            return volumeSFX;
        }
    }

    #endregion



}
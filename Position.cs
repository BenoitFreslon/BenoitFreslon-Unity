using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Position : MonoBehaviour {

	public static float screenWidthMin = 480f;
	public static float screenHeightMin = 320f;


	public enum PositionType {
		Absolute,
		Relative,
		Percentage
	}

	public PositionType type = PositionType.Absolute;

	public enum PositionRelative {
		Top,
		TopLeft,
		TopRight,
		Center,
		Bottom,
		BottomLeft,
		BottomRight,
		Left,
		Right
	}

	public PositionRelative relativeTo = PositionRelative.Center;
	
	// Use this for initialization
	void Start () {
		//GetComponent<TextMesh>()
		if (type == PositionType.Percentage) {
			Debug.Log("Screen.width " + Screen.width);
			float x = transform.position.x + transform.position.x * (Screen.width - screenWidthMin) / screenWidthMin;
			float y = transform.position.y + transform.position.y * (Screen.height - screenHeightMin) / screenHeightMin;
			transform.position = new Vector3 (x, y, transform.position.y);

		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}


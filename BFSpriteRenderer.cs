﻿using UnityEngine;
using System.Collections;

public class BFSpriteRenderer : MonoBehaviour
{
	public string PathSprite = null;
	void Awake ()
	{
		if (PathSprite != null) {
			GetComponent<SpriteRenderer> ().sprite = BFUtils.LoadSpriteFromResources (PathSprite);
		}
	}
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

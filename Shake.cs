﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour
{
	public bool IsShaking = false;
	public bool IsPaused = false;

	[HideInInspector]
	public Vector3 oldPos;

	private  float amplitude;
	private  float attenuation;
	private  int directionX;
	private  int directionY;
	// Use this for initialization
	void Awake ()
	{
		oldPos = transform.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if ( !IsShaking )
			return;
		if ( IsPaused )
			return;
		if ( amplitude <= 0.001f ) {
			Stop ();
		} else {
			
			transform.position = new Vector3 ( oldPos.x + directionX * amplitude, oldPos.y + directionY * amplitude, oldPos.z );
			amplitude *= attenuation;
			randomDirection ();
		}
	}

	public void Play ( float pAmplitude = 0.5f, float pAttenuation = .9f )
	{
		Debug.Log ( "oldPos: " + oldPos );
		IsShaking = true;
		transform.position = oldPos;

		amplitude = pAmplitude;
		attenuation = pAttenuation;
		randomDirection ();
	}

	void randomDirection ()
	{
		directionX = Random.Range ( -1, 1 );
		directionY = Random.Range ( -1, 1 );
	}

	public void Pause ()
	{
		IsPaused = true;
	}

	public void Resume ()
	{
		IsPaused = false;
	}

	public void Stop ()
	{
		IsShaking = false;
		transform.position = oldPos;
	}
}

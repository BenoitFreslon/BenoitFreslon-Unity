﻿using UnityEngine;
using System.Collections;

public class TouchScreenKeyboardManager : MonoBehaviour
{
	private const string TAG = "[TouchScreenKeyboardManager]: ";

	#if !UNITY_STANDALONE && !UNITY_WEBGL

	protected TouchScreenKeyboard keyboard;

	public event System.Action OnOpen;

	public event System.Action<string> OnDone;

	public event System.Action OnCancel;

	public event System.Action<string> OnKey;

	public bool IsOpened = false;

	protected string curText = "";


	public TouchScreenKeyboardManager ()
	{
		//Debug.Log (TAG+"#TouchScreenKeyboardManager# Constructor");
	}

	void Awake ()
	{

	}
	// Use this for initialization
	void Start ()
	{
		name = "~TouchScreenKeyboardManager";

	}

	void Update ()
	{
		if ( keyboard == null )
			return;

		if ( TouchScreenKeyboard.visible ) {
			//Debug.Log ( keyboard );
			if ( keyboard.active && !IsOpened ) {
				IsOpened = true;
				if ( OnOpen != null )
					OnOpen ();
			}
			if ( keyboard.text != curText ) {
				curText = keyboard.text;
				if ( OnKey != null )
					OnKey ( keyboard.text );
			}
		} else {
			if ( keyboard.done && IsOpened ) {
				IsOpened = false;
				if ( OnDone != null )
					OnDone ( keyboard.text );

				Close ();
			} else if ( keyboard.wasCanceled && IsOpened ) {
				IsOpened = false;
				if ( OnCancel != null )
					OnCancel ();

				Close ();
			}
			//Debug.Log (keyboard.done, keyboard.active, keyboard.targetDisplay, keyboard.text, keyboard.wasCanceled);
		}



#if UNITY_EDITOR
		if ( Input.inputString != "" ) {
			if ( Input.GetKeyDown ( KeyCode.Return ) ) {
				//Debug.Log ( "TouchScreenKeyboardManager: OnDone" );
				if ( OnDone != null )
					OnDone ( keyboard.text );
				Close ();
				return;
			} else if ( Input.GetKeyDown ( KeyCode.Escape ) ) {
				//Debug.Log ( "TouchScreenKeyboardManager: OnCancel" );
				if ( OnCancel != null )
					OnCancel ();
				Close ();
				return;
			}
			keyboard.text += Input.inputString;
			curText = keyboard.text;
			Debug.Log ( TAG + "Input.inputString: " + Input.inputString + "\t" + keyboard.text );

			if ( OnKey != null )
				OnKey ( keyboard.text );

		}
#endif
	}

	public void Open ( string text = "", TouchScreenKeyboardType keyboardType = TouchScreenKeyboardType.Default, bool autocorrection = false, bool multiline = false, bool secure = false, bool alert = false, string textPlaceholder = "" )
	{
		Close ();
		curText = text;
		keyboard = TouchScreenKeyboard.Open ( text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder );
		Debug.Log ( TAG + "Open: " + text + " " + keyboardType + " " + autocorrection + " " + multiline + " " + secure + " " + alert + " " + textPlaceholder );
#if UNITY_EDITOR
		if ( OnOpen != null )
			OnOpen ();
#endif
	}

	public void Close ()
	{
		if ( keyboard != null )
			keyboard.active = false;
		keyboard = null;
		IsOpened = false;
		curText = "";
	}
	#endif
}
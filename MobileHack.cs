﻿using UnityEngine;
using System.Collections;

public class MobileHack : MonoBehaviour
{

    public int Framerate = 60;

    // Use this for initialization
    static MobileHack instance;

    void Awake ()
    {
        Application.targetFrameRate = Framerate;
    }

    void Start ()
    {
        name = "~MobileHack";
        if ( !instance ) {
            instance = this;
            DontDestroyOnLoad ( gameObject );
        } else {
            DestroyImmediate ( gameObject );
        }

    }

    /*
    // Update is called once per frame
    void Update ()
    {
#if UNITY_ANDROID
        if ( Application.platform == RuntimePlatform.Android && BFDevice.AndroidVersion <= 10 ) {
            if ( Input.GetKey ( KeyCode.Home ) || Input.GetKey ( KeyCode.Escape ) || Input.GetKey ( KeyCode.Menu ) ) {
                Application.Quit ();
                return;
            }
        }
#endif
    }
    */
#if UNITY_ANDROID
    void OnApplicationPause ( bool pausestatus )
    {
        /*
		// Problem with GooglePlay login (loop)
		if (Application.platform == RuntimePlatform.Android && pausestatus && BFDevice.AndroidVersion <= 10) {
			
			Application.Quit ();
			return;
		}
		*/

    }
#endif
}
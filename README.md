# MonoDevelop
## Problème de paramètre par défaut
* MonoDevelop
* Options
* Unity/Debugger
* Décocher **Build project in MonoDevelop**

# Unity Log

* macOS	`~/Library/Logs/Unity/Editor.log`
* Windows Vista/7	`C:\Users\username\AppData\Local\Unity\Editor\Editor.log`

# LeanTween
## Ajouter un paramètre

```
int pouet = 666;
LeanTween.delayedCall (gameObject, 1f, testage).setOnCompleteParam (int as object);

void testage (object obj)
{
	int i = obj as int;	
} 
```

## Ajouter plusieurs paramètres
```
int[] tab = new int[]{1,2,3};
LeanTween.delayedCall (gameObject, 1f, testage).setOnCompleteParam (tab as object);

void testage (object obj)
{
	int[] tab = obj as int[];
	Debug.Log (tab [0]);
} 
```
## Shake
```
LeanTween.move ( gameObject, new Vector3 ( 1, 1, 0 ), 4 )
	.setEase ( LeanTweenType.easeShake )
	.setLoopClamp ()
	.setRepeat ( 8 ).setUseFrames ( true );
```

# Icône
# Screenshots

# Creation d'une application

## Google Analytics
* Créer une application sur [GoogleAnalytics](https://www.google.com/analytics/web/#management/Settings/a37301379w68510180p70542169/)
* Récupérer l'ID
* Ajouter les ID dans Unity
* Cocher **Use Player Settings**

# RevMob
# AdColony
# UnityAds
# Chartboost
* Ajouter 2 applications sur le [site](https://dashboard.chartboost.com/apps/create)
* Récupérer les ID Android et iOS
* Ajouter une campagne pour chaque plateforme
* Importer le [Package Unity](https://answers.chartboost.com/hc/en-us/articles/200780379-Unity-Integrations) ou [GitHub](https://github.com/ChartBoost/unity)
* Ajouter les ID dans le fichier **Constants.cs**

### Problème avec export Web
When I try to build a web version of my game with the ChartBoost framework I got compile errors.

To fix this error I have to modify the CBExternal.cs

I have to add || UNITY_WEBPLAYER at line 27:

## iTunes connect
* Créer un iD sur [iOSDevPortail](https://developer.apple.com/account/ios/identifiers/bundle/bundleList.action)
* Créer et télécharger 3 **Provisioning Profiles** :
	- **Development**
	- **Ad Hoc**
	- **App Store**
* Créer une application sur [iTunesConnect](https://itunesconnect.apple.com/WebObjects/iTunesConnect.woa/wo/4.0.0.13.7.2.7.9.1.1.3.2.1.3.0.1.3.1)
* Créer un achat inApp
* Ajouter l'achat InApp dans **iOSNativeSettings**

## Vidéo
## Android console

## Social
Sur AndroidNative et iOSNative la texture pour le partage Facebook ou Twitter doit être en type **Advanced** et cocher **Read/Write enabled**.

## AndroiDNativeSettings
* Billing Settings
* Base64 key [Console Android](https://play.google.com/apps/publish/) > Services et API

## Constants
* base64Key
* IAP

# Mise à jour Unity
## Assets
* Unity
* GoogleAnalytics
* AndroidNative
	- Attention aux Fichiers à ne pas supprimer lors de la maj:
 		- Assets/Plugins/Android/AndroidManifest.xml
 		- Assets/Plugins/Android/res/values/ids.xml
* iOSNative
	- Modifier le fichier DeviceTokenListener pour décommenter `FixedUpdate` et commenter `IOSMessage.Create("Push registration disabled", "Uncomment 'FixedUpdate' function inside DeviceTokenListner class");`

# Exports nouvelle version

## Unity
1. Modifier version dans **iOSNativeSettings**
2. Modifier version dans **AndroidNativeSettings**
3. Modifier version dans le **AndroidManifest.xml**

## iOS
1. Nouvelle version sur iTunes
2. Modifier la version sur Unity
3. Export pour Xcode
4. Archive
5. Upload iTunes
6. IOSNativeSettings
7. - Apple Id
8. - Billing Settings

## Android
1. Modifier version Android & build
2. Exporter le .apk
3. Uploader sur la console Android

# TextMesh

## Nouvelle ligne
WIP

# DEBUG

* Mac OS X	~/Library/Logs/Unity/Editor.log
* Windows XP	C:\Documents and Settings\username\Local Settings\Application Data_\Unity\Editor\Editor.log
* Windows Vista/7	C:\Users\username\AppData\Local\Unity\Editor\Editor.log


# GIT

[http://stackoverflow.com/questions/18225126/how-to-use-git-for-unity3d-source-control](http://stackoverflow.com/questions/18225126/how-to-use-git-for-unity3d-source-control)

## gitignore file :

```
# =============== #
# Unity generated #
# =============== #
Library/
Temp/

# ===================================== #
# Visual Studio / MonoDevelop generated #
# ===================================== #
ExportedObj/
Obj/
*.svd
*.userprefs
.csproj
*.pidb
*.suo
.sln
*.user
*.unityproj
*.booproj

# ============ #
# OS generated #
# ============ #
.DS_Store
.DS_Store?
._*
.Spotlight-V100
.Trashes
ehthumbs.db
Thumbs.db
```
# MOBILE

* Pas de compression des icônes. Format GUI true color.
* Une icône par taille.

##ANDROID

### UI Thread

Call method on UI Thread

```
AndroidJavaClass unityPlayer = new AndroidJavaClass ( "com.unity3d.player.UnityPlayer" );
AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject> ( "currentActivity" );
        activity.Call ( "runOnUiThread", new AndroidJavaRunnable ( runOnUiThread ) );

void runOnUiThread ()
{
    Debug.Log ( "I'm running on the Java UI thread!" );
}
```

### Erreurs Java

#### Unable to list target platforms
When you build for Android

1. just open [https://developer.android.com/studio/index.html](https://developer.android.com/studio/index.html)
2. scrolldown to the bottom of that page
3. find Windows "tools_r25.2.3-windows.zip"
download and unzip it
4. remove and replace your folder "SDK" + "/tools"
5. Enjoy!
6. By "SDK Manager" do upgrade to 25.2.5
7. Now works all: Eclipse, Android Studio, Unity.
Enjoy again!

####Erreur Java Windows : Error building Player: Win32Exception: 32bits
```
Erreur Java type : Error building Player: Win32Exception: ApplicationName='C:\Program Files (x86)\Java\jre7\bin\javac.exe',
Installer Java JDK 32bits et non 64bitrreur Java type : Error building Player: Win32Exception: ApplicationName='C:\Program Files (x86)\Java\jre7\bin\javac.exe',
```
* Installer Java JDK 32bits et non 64bit
* Réinstaller Android SDK

####Erreur Java : Error building Player: CommandInvokationFailure
```
Error building Player: CommandInvokationFailure: Unable to convert classes into dex format. See the Console for details.
/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home/bin/java -Xmx1024M -Dcom.android.sdkmanager.toolsdir="/Users/Benoit/Workshop/Libs/Android/adt-bundle-mac-x86_64-20140321/sdk/tools" -Dfile.encoding=UTF8 -jar "/Applications/Unity/Unity.app/Contents/BuildTargetTools/AndroidPlayer/sdktools.jar" -

stderr[

UNEXPECTED TOP-LEVEL EXCEPTION:
java.lang.IllegalArgumentException: already added: Lcom/google/android/gms/internal/e;
```
* En général la lin java google-player.jar est déjà ajoutée.

####Erreur Java : Error building Player: Win32Execption: zipalign

```
Error building Player: Win32Exception: ApplicationName='/Users/Benoit/Workshop/Libs/Android/adt-bundle-mac-x86_64-20140321/sdk/tools/zipalign', CommandLine='4 "/Users/Benoit/Workshop/Projets/30FoodChains/Temp/StagingArea/Package_unaligned.apk" "/Users/Benoit/Workshop/Projets/30FoodChains/Temp/StagingArea/Package.apk"', CurrentDirectory='Temp/StagingArea'
```

* Problème du fichier zipalign manquant.
* Il faut rétélécharger le SDK Android Mac.

####Erreur Java : ERROR: No argument supplied for '--extra-packages' option Android Asset Packaging Tool



### Forcer 60 ips



#### Dans Unity

	Application.targetFrameRate = 60;
	#if UNITY_EDITOR
	QualitySettings.vSyncCount = 0;  // VSync must be disabled
	#endif
	

#### Log Android

##### Android device Monitor**
Ouvrir `android-sdk-macosx/tools/monitor`

Clear the log

	/FOLDER_OF_THE_ANDROID_SDK/sdk/platform-tools/adb logcat
	adb logcat -c (clears the log)

#### Afficher dans un fichier



	adb logcat > yourlog.txt(dumps the log to a textfile called yourlog.txt)

#### Afficher uniquement les logs Unity
	adb logcat -s Unity ActivityManager PackageManager dalvikvm DEBUG

##Configuration Android

__Android/AndroidManifest.xml__

* APP_ID
* Android/res/values/ids.xml
* Changer les ID des classements et des succès

## iOS

### Erreurs

#### 
```

Undefined symbols for architecture arm64:
  "__chartBoostTrackEventWithValueAndMetadata", referenced from:
      _ChartBoostBinding__chartBoostTrackEventWithValueAndMetadata_m2552723552 in Bulk_Assembly-CSharp-firstpass_0.o
     (maybe you meant: _ChartBoostBinding__chartBoostTrackEventWithValueAndMetadata_m2552723552)
  "__chartBoostForceOrientation", referenced from:

```

#### dymultil

	dsymutil failed with exit code 11

* __Target > Build Settings > Debug Information Format DWARF__

### Frameworks

__Voir les Frameworks des SDK__

### Other Linker Flags
* -ObjC
* -fobjc-arc

### SDK

* Chartboost : [https://help.chartboost.com/documentation/unity](https://help.chartboost.com/documentation/unity)
* AdColony : [https://github.com/AdColony/AdColony-iOS-SDK/wiki/Xcode-Project-Setup](https://github.com/AdColony/AdColony-iOS-SDK/wiki/Xcode-Project-Setup)
* RevMob : [http://sdk.revmobmobileadnetwork.com/unity.html](http://sdk.revmobmobileadnetwork.com/unity.html])

### RevMob
[http://sdk.revmobmobileadnetwork.com/unity.html](http://sdk.revmobmobileadnetwork.com/unity.html)

#### iOS

* SystemConfiguration
* StoreKit
* AdSupport


#### Android


###ChartBoost
[https://answers.chartboost.com/hc/en-us/articles/201219745-Unity-SDK-Download(https://answers.chartboost.com/hc/en-us/articles/201219745-Unity-SDK-Download)
#### iOS

* QuartzCore
* SystemConfiguration
* CoreGraphics
* AdSupport
* StoreKit
* CoreMedia (SDK v4.4+)
* AVFoundation (SDK v4.4+)
* CoreData (SDK v4.4+)

###AdColony

* UnityADC `-fno-objc-arc`

* Other Linking Flags : `-ObjC`

[https://github.com/AdColony/AdColony-Unity-SDK](https://github.com/AdColony/AdColony-Unity-SDK)
#### iOS
* libz.1.2.5.dylib
* AdColony.framework
* AdSupport.framework (Set to Optional)
* AudioToolbox.framework
* AVFoundation.framework
* CoreGraphics.framework
* CoreMedia.framework
* CoreTelephony.framework
* EventKit.framework
* EventKitUI.framework
* MediaPlayer.framework
* MessageUI.framework
* QuartzCore.framework
* Social.framework (Set to Optional)
* StoreKit.framework (Set to Optional)
* SystemConfiguration.framework

#### Android


## Compiler en java
Commande pour compiler les .java en .jar

	"C:\Program Files\Java\jdk1.8.0_05\bin\jar" cvf fichier.jar src


# LOCALIZATION
## CSV

**NB: Ne pas utiliser le charactère "virgule" (,) dans ce document. Utiliser plutôt le format *TSV* sinon.**

* Créer un document Google Spreadsheet
* Aouter une colonne vide
* Ajouter les colonnes suivantes avec les codes de la langue :

```
				English		French
				en			fr
	keyword		en_EN		fr_FR
	hello		Hello!		Bonjour!
	play		Play		Jouer
	quit		Quit		Quitter
```

* Exporter le fichier en .csv dans le dossier **Resources** de Unity
* Dans Unity ajouter le code ```Localization.AddCSV("NomDuFichier.csv", Application.systemLanguage.ToString ());```


# TODO

* Compilation Java
* Installer Java SDK
* Installer Java pour Android SDK


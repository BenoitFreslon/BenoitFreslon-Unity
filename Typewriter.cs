﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEditor;

public class Typewriter : MonoBehaviour
{

    public bool UseColor = false;
    public string TextToWrite = "";
    public string EnddingChar = "";
    public float Delay = 0.1f;
    public bool UseFrames = false;
    public bool DoubleSpeed = false;

    List<string> textList = new List<string> ();
    List<string> htmlClosed = new List<string> ();
    List<string> htmlOpened = new List<string> ();

    int curLetterId = 0;
    string leftText = "";
    string rightText = "";

    public bool IsWriting = false;
    public bool IsPaused = false;

    public event System.Action<string> OnWriteText = delegate { };
    public event System.Action OnWritingEnded = delegate { };

    // Use this for initialization
    void Start ()
    {

    }

    int t = 0;
    // Update is called once per frame
    void Update ()
    {

    }

    public void Play ()
    {
        Play ( TextToWrite );
    }

    public void Play ( string txt )
    {
        Stop ();
        Reset ();
        TextToWrite = txt;
        char[] charArray = TextToWrite.ToCharArray ();
        foreach ( char c in charArray )
            textList.Add ( c.ToString () );
        curLetterId = 0;

        LeanTween.delayedCall ( gameObject, Delay, writeLetter ).setLoopPingPong ( TextToWrite.Length - 1 ).setOnComplete ( writeLetter ).setOnCompleteOnRepeat ( true ).setUseFrames ( UseFrames );
        if ( DoubleSpeed )
            LeanTween.delayedCall ( gameObject, Delay, writeLetter ).setLoopPingPong ( TextToWrite.Length - 1 ).setOnComplete ( writeLetter ).setOnCompleteOnRepeat ( true ).setUseFrames ( UseFrames );
        IsWriting = true;
    }

    string addHTML ()
    {
        bool willClose = false;
        string html = textList[ curLetterId ];

        if ( textList[ curLetterId + 1 ] == "/" ) {
            willClose = true;
        }
        string curTag = textList[ curLetterId + 2 ];

        for ( curLetterId++; curLetterId < textList.Count; curLetterId++ ) {

            html += textList[ curLetterId ];

            if ( textList[ curLetterId ] == ">" ) {
                curLetterId++;

                if ( willClose ) {
                    if ( curTag == "c" ) {
                        htmlClosed.Remove ( "</color>" );
                        htmlOpened.Remove ( "<color>" );
                    } else if ( curTag == "b" ) {
                        htmlClosed.Remove ( "</b>" );
                        htmlOpened.Remove ( "<b>" );
                    } else if ( curTag == "i" ) {
                        htmlClosed.Remove ( "</i>" );
                        htmlOpened.Remove ( "<i>" );
                    }

                } else {
                    if ( html.Contains ( "<color" ) ) {
                        htmlClosed.Add ( "</color>" );
                        htmlOpened.Add ( "<color>" );
                    } else if ( html.Contains ( "<b>" ) ) {
                        htmlClosed.Add ( "</b>" );
                        htmlOpened.Add ( "<b>" );
                    } else if ( html.Contains ( "<i>" ) ) {
                        htmlClosed.Add ( "</i>" );
                        htmlOpened.Add ( "<i>" );
                    }
                }
                break;
            }
        }
        if ( curLetterId >= textList.Count ) {
            End ();
            return "";
        }
        //Debug.Log ("html: " + html);
        return html;
    }

    void writeLetter ()
    {
        if ( IsPaused ) return;

        string letter = textList[ curLetterId ];

        bool isHtml = false;
        if ( letter == "<" ) {
            isHtml = true;
            letter = "";
        }
        while ( curLetterId < textList.Count && textList[ curLetterId ] == "<" ) {
            letter += addHTML ();
        }

        //Debug.Log (curLetterId + " " + textList.Count);
        if ( curLetterId >= textList.Count ) {
            End ();
            return;
        }

        if ( isHtml )
            letter += textList[ curLetterId ];

        leftText += letter;

        rightText = "";
        if ( UseColor ) {

            for ( int i = curLetterId + 1; i < textList.Count; i++ ) {
                rightText += textList[ i ];
            }
            rightText = removeHtml ( rightText );

            if ( curLetterId < textList.Count ) {


                // Balise ouvertes
                string startHtml = "";
                for ( int i = 0; i < htmlOpened.Count; i++ ) {
                    if ( !htmlOpened[ i ].Contains ( "<color" ) )
                        startHtml += htmlOpened[ i ];
                }

                rightText = startHtml + rightText;

                rightText = "<color=#00000000>" + rightText;

                rightText += "</color>";

                string endHtml = "";
                for ( int i = htmlClosed.Count - 1; i >= 0; i-- ) {
                    endHtml += htmlClosed[ i ];
                }
                rightText = endHtml + rightText;
            }

        } else {

            for ( int i = htmlClosed.Count - 1; i >= 0; i-- )
                rightText += htmlClosed[ i ];
        }


        string curText = leftText + EnddingChar + rightText;
        writeText ( curText );

        curLetterId++;
        if ( curLetterId >= textList.Count )
            End ();
    }

    string removeHtml ( string s )
    {
        //		s.Replace ( "<b>", "" );
        //		s.Replace ( "</b>", "" );
        //		s.Replace ( "<i>", "" );
        string temp = "";
        if ( string.IsNullOrEmpty ( s ) ) {
            return "";
        }
        s = s.Replace ( "</color>", "" );
        char[] arr = s.ToCharArray ();
        bool isHtml = false;
        string c;
        for ( int i = 0; i < arr.Length; i++ ) {
            c = arr[ i ].ToString ();
            if ( c == "<" && arr[ i + 1 ].ToString () == "c" ) {
                isHtml = true;
            }
            if ( !isHtml )
                temp += c;
            if ( isHtml && c == ">" )
                isHtml = false;
        }

        return temp;
    }

    void writeText ( string curText )
    {
        if ( GetComponent<Text> () )
            GetComponent<Text> ().text = curText;
        if ( GetComponent<TextMesh> () )
            GetComponent<TextMesh> ().text = curText;
        if ( OnWriteText != null )
            OnWriteText ( curText );
    }

    public void Pause ()
    {
        if ( IsWriting ) {
            LeanTween.pause ( gameObject );
            IsWriting = false;
        }
        IsPaused = true;
    }

    public void Resume ()
    {
        if ( IsWriting == false ) {
            LeanTween.resume ( gameObject );
            IsWriting = true;
        }
        IsPaused = false;
    }

    public void End ()
    {
        OnWritingEnded ();
        Stop ();
    }

    public void Stop ()
    {
        LeanTween.cancel ( gameObject );
        IsWriting = false;
        writeText ( TextToWrite );
    }

    void OnDestroy ()
    {
        LeanTween.cancel ( gameObject );
        IsWriting = false;
        OnWritingEnded = delegate {
        };
        OnWriteText = delegate {
        };
    }

    public void Clear ()
    {
        if ( GetComponent<Text> () )
            GetComponent<Text> ().text = "";
        if ( GetComponent<TextMesh> () )
            GetComponent<TextMesh> ().text = "";
    }

    public void Reset ()
    {
        htmlClosed.Clear ();
        htmlOpened.Clear ();
        textList.Clear ();
        curLetterId = 0;
        leftText = "";
        rightText = "";
        IsWriting = false;

        //Debug.Log ( "Reset" );
        Clear ();
    }
}

﻿using System;
using UnityEngine;

public enum AndroidFeature {
	FEATURE_APP_WIDGETS,
	FEATURE_AUDIO_LOW_LATENCY,
	FEATURE_AUDIO_OUTPUT,
	FEATURE_AUDIO_PRO,
	FEATURE_BACKUP,
	FEATURE_BLUETOOTH,
	FEATURE_BLUETOOTH_LE,
	FEATURE_CAMERA,
	FEATURE_CAMERA_ANY,
	FEATURE_CAMERA_AUTOFOCUS,
	FEATURE_CAMERA_CAPABILITY_MANUAL_POST_PROCESSING,
	FEATURE_CAMERA_CAPABILITY_MANUAL_SENSOR,
	FEATURE_CAMERA_CAPABILITY_RAW,
	FEATURE_CAMERA_EXTERNAL,
	FEATURE_CAMERA_FLASH,
	FEATURE_CAMERA_FRONT,
	FEATURE_CAMERA_LEVEL_FULL,
	FEATURE_CONNECTION_SERVICE,
	FEATURE_CONSUMER_IR,
	FEATURE_DEVICE_ADMIN,
	FEATURE_ETHERNET,
	FEATURE_FAKETOUCH,
	FEATURE_FAKETOUCH_MULTITOUCH_DISTINCT,
	FEATURE_FAKETOUCH_MULTITOUCH_JAZZHAND,
	FEATURE_FINGERPRINT,
	FEATURE_FREEFORM_WINDOW_MANAGEMENT,
	FEATURE_GAMEPAD,
	FEATURE_HIFI_SENSORS,
	FEATURE_HOME_SCREEN,
	FEATURE_INPUT_METHODS,
	FEATURE_LEANBACK,
	FEATURE_LIVE_TV,
	FEATURE_LIVE_WALLPAPER,
	FEATURE_LOCATION,
	FEATURE_LOCATION_GPS,
	FEATURE_LOCATION_NETWORK,
	FEATURE_MANAGED_USERS,
	FEATURE_MICROPHONE,
	FEATURE_MIDI,
	FEATURE_NFC,
	FEATURE_NFC_HOST_CARD_EMULATION,
	FEATURE_NFC_HOST_CARD_EMULATION_NFCF,
	FEATURE_OPENGLES_EXTENSION_PACK,
	FEATURE_PICTURE_IN_PICTURE,
	FEATURE_PRINTING,
	FEATURE_SCREEN_LANDSCAPE,
	FEATURE_SCREEN_PORTRAIT,
	FEATURE_SECURELY_REMOVES_USERS,
	FEATURE_SENSOR_ACCELEROMETER,
	FEATURE_SENSOR_AMBIENT_TEMPERATURE,
	FEATURE_SENSOR_BAROMETER,
	FEATURE_SENSOR_COMPASS,
	FEATURE_SENSOR_GYROSCOPE,
	FEATURE_SENSOR_HEART_RATE,
	FEATURE_SENSOR_HEART_RATE_ECG,
	FEATURE_SENSOR_LIGHT,
	FEATURE_SENSOR_PROXIMITY,
	FEATURE_SENSOR_RELATIVE_HUMIDITY,
	FEATURE_SENSOR_STEP_COUNTER,
	FEATURE_SENSOR_STEP_DETECTOR,
	FEATURE_SIP,
	FEATURE_SIP_VOIP,
	FEATURE_TELEPHONY,
	FEATURE_TELEPHONY_CDMA,
	FEATURE_TELEPHONY_GSM,
	FEATURE_TELEVISION,
	FEATURE_TOUCHSCREEN,
	FEATURE_TOUCHSCREEN_MULTITOUCH,
	FEATURE_TOUCHSCREEN_MULTITOUCH_DISTINCT,
	FEATURE_TOUCHSCREEN_MULTITOUCH_JAZZHAND,
	FEATURE_USB_ACCESSORY,
	FEATURE_USB_HOST,
	FEATURE_VERIFIED_BOOT,
	FEATURE_VR_MODE,
	FEATURE_VR_MODE_HIGH_PERFORMANCE,
	FEATURE_VULKAN_HARDWARE_LEVEL,
	FEATURE_VULKAN_HARDWARE_VERSION,
	FEATURE_WATCH,
	FEATURE_WEBVIEW,
	FEATURE_WIFI,
	FEATURE_WIFI_DIRECT
}

public class AndroidFeatureMethods
{
	// https://developer.android.com/reference/android/content/pm/PackageManager.html
	/*
	public static readonly string FEATURE_APP_WIDGETS = "FEATURE_APP_WIDGETS";
	public static readonly string FEATURE_AUDIO_LOW_LATENCY = "FEATURE_AUDIO_LOW_LATENCY";
	public static readonly string FEATURE_AUDIO_OUTPUT = "FEATURE_AUDIO_OUTPUT";
	public static readonly string FEATURE_AUDIO_PRO = "FEATURE_AUDIO_PRO";
	public static readonly string FEATURE_BACKUP = "FEATURE_BACKUP";
	public static readonly string FEATURE_BLUETOOTH = "FEATURE_BLUETOOTH";
	public static readonly string FEATURE_BLUETOOTH_LE = "FEATURE_BLUETOOTH_LE";
	public static readonly string FEATURE_CAMERA = "FEATURE_CAMERA";
	public static readonly string FEATURE_CAMERA_ANY = "FEATURE_CAMERA_ANY";
	public static readonly string FEATURE_CAMERA_AUTOFOCUS = "FEATURE_CAMERA_AUTOFOCUS";
	public static readonly string FEATURE_CAMERA_CAPABILITY_MANUAL_POST_PROCESSING = "FEATURE_CAMERA_CAPABILITY_MANUAL_POST_PROCESSING";
	public static readonly string FEATURE_CAMERA_CAPABILITY_MANUAL_SENSOR = "FEATURE_CAMERA_CAPABILITY_MANUAL_SENSOR";
	public static readonly string FEATURE_CAMERA_CAPABILITY_RAW = "FEATURE_CAMERA_CAPABILITY_RAW";
	public static readonly string FEATURE_CAMERA_EXTERNAL = "FEATURE_CAMERA_CAPABILITY_RAW";
	public static readonly string FEATURE_CAMERA_FLASH = "FEATURE_CAMERA_FLASH";
	public static readonly string FEATURE_CAMERA_FRONT = "FEATURE_CAMERA_FRONT";
	public static readonly string FEATURE_CAMERA_LEVEL_FULL = "FEATURE_CAMERA_LEVEL_FULL";
	public static readonly string FEATURE_CONNECTION_SERVICE = "FEATURE_CONNECTION_SERVICE";
	public static readonly string FEATURE_CONSUMER_IR = "FEATURE_CONSUMER_IR";
	public static readonly string FEATURE_DEVICE_ADMIN = "FEATURE_DEVICE_ADMIN";
	public static readonly string FEATURE_ETHERNET = "FEATURE_ETHERNET";
	public static readonly string FEATURE_FAKETOUCH = "FEATURE_FAKETOUCH";
	public static readonly string FEATURE_FAKETOUCH_MULTITOUCH_DISTINCT = "FEATURE_FAKETOUCH_MULTITOUCH_DISTINCT";
	public static readonly string FEATURE_FAKETOUCH_MULTITOUCH_JAZZHAND = "FEATURE_FAKETOUCH_MULTITOUCH_JAZZHAND";
	public static readonly string FEATURE_FINGERPRINT = "FEATURE_FINGERPRINT";
	public static readonly string FEATURE_FREEFORM_WINDOW_MANAGEMENT = "FEATURE_FREEFORM_WINDOW_MANAGEMENT";
	public static readonly string FEATURE_GAMEPAD = "FEATURE_GAMEPAD";
	public static readonly string FEATURE_HIFI_SENSORS = "FEATURE_HIFI_SENSORS";
	public static readonly string FEATURE_HOME_SCREEN = "FEATURE_HOME_SCREEN";
	public static readonly string FEATURE_INPUT_METHODS = "FEATURE_INPUT_METHODS";
	public static readonly string FEATURE_LEANBACK = "FEATURE_LEANBACK";
	public static readonly string FEATURE_LIVE_TV = "FEATURE_LIVE_TV";
	public static readonly string FEATURE_LIVE_WALLPAPER = "FEATURE_LIVE_WALLPAPER";
	public static readonly string FEATURE_LOCATION = "FEATURE_LOCATION";
	public static readonly string FEATURE_LOCATION_GPS = "FEATURE_LOCATION_GPS";
	public static readonly string FEATURE_LOCATION_NETWORK = "FEATURE_LOCATION_NETWORK";
	public static readonly string FEATURE_MANAGED_USERS = "FEATURE_MANAGED_USERS";
	public static readonly string FEATURE_MICROPHONE = "FEATURE_MICROPHONE";
	public static readonly string FEATURE_MIDI = "FEATURE_MIDI";
	public static readonly string FEATURE_NFC = "FEATURE_NFC";
	public static readonly string FEATURE_NFC_HOST_CARD_EMULATION = "FEATURE_NFC_HOST_CARD_EMULATION";
	public static readonly string FEATURE_NFC_HOST_CARD_EMULATION_NFCF = "FEATURE_NFC_HOST_CARD_EMULATION_NFCF";
	public static readonly string FEATURE_OPENGLES_EXTENSION_PACK = "FEATURE_OPENGLES_EXTENSION_PACK";
	public static readonly string FEATURE_PICTURE_IN_PICTURE = "FEATURE_PICTURE_IN_PICTURE";
	public static readonly string FEATURE_PRINTING = "FEATURE_PRINTING";
	public static readonly string FEATURE_SCREEN_LANDSCAPE = "FEATURE_SCREEN_LANDSCAPE";
	public static readonly string FEATURE_SCREEN_PORTRAIT = "FEATURE_SCREEN_PORTRAIT";
	public static readonly string FEATURE_SECURELY_REMOVES_USERS = "FEATURE_SECURELY_REMOVES_USERS";
	public static readonly string FEATURE_SENSOR_ACCELEROMETER = "FEATURE_SENSOR_ACCELEROMETER";
	public static readonly string FEATURE_SENSOR_AMBIENT_TEMPERATURE = "FEATURE_SENSOR_AMBIENT_TEMPERATURE";
	public static readonly string FEATURE_SENSOR_BAROMETER = "FEATURE_SENSOR_BAROMETER";
	public static readonly string FEATURE_SENSOR_COMPASS = "FEATURE_SENSOR_COMPASS";
	public static readonly string FEATURE_SENSOR_GYROSCOPE = "FEATURE_SENSOR_GYROSCOPE";
	public static readonly string FEATURE_SENSOR_HEART_RATE = "FEATURE_SENSOR_HEART_RATE";
	public static readonly string FEATURE_SENSOR_HEART_RATE_ECG = "FEATURE_SENSOR_HEART_RATE_ECG";
	public static readonly string FEATURE_SENSOR_LIGHT = "FEATURE_SENSOR_LIGHT";
	public static readonly string FEATURE_SENSOR_PROXIMITY = "FEATURE_SENSOR_PROXIMITY";
	public static readonly string FEATURE_SENSOR_RELATIVE_HUMIDITY = "FEATURE_SENSOR_RELATIVE_HUMIDITY";
	public static readonly string FEATURE_SENSOR_STEP_COUNTER = "FEATURE_SENSOR_STEP_COUNTER";
	public static readonly string FEATURE_SENSOR_STEP_DETECTOR = "FEATURE_SENSOR_STEP_DETECTOR";
	public static readonly string FEATURE_SIP = "FEATURE_SIP";
	public static readonly string FEATURE_SIP_VOIP = "FEATURE_SIP_VOIP";
	public static readonly string FEATURE_TELEPHONY = "FEATURE_TELEPHONY";
	public static readonly string FEATURE_TELEPHONY_CDMA = "FEATURE_TELEPHONY_CDMA";
	public static readonly string FEATURE_TELEPHONY_GSM = "FEATURE_TELEPHONY_GSM";
	public static readonly string FEATURE_TELEVISION = "FEATURE_TELEVISION";
	public static readonly string FEATURE_TOUCHSCREEN = "FEATURE_TOUCHSCREEN";
	public static readonly string FEATURE_TOUCHSCREEN_MULTITOUCH = "FEATURE_TOUCHSCREEN_MULTITOUCH";
	public static readonly string FEATURE_TOUCHSCREEN_MULTITOUCH_DISTINCT = "FEATURE_TOUCHSCREEN_MULTITOUCH_DISTINCT";
	public static readonly string FEATURE_TOUCHSCREEN_MULTITOUCH_JAZZHAND = "FEATURE_TOUCHSCREEN_MULTITOUCH_JAZZHAND";
	public static readonly string FEATURE_USB_ACCESSORY = "FEATURE_USB_ACCESSORY";
	public static readonly string FEATURE_USB_HOST = "FEATURE_USB_HOST";
	public static readonly string FEATURE_VERIFIED_BOOT = "FEATURE_VERIFIED_BOOT";
	public static readonly string FEATURE_VR_MODE = "FEATURE_VR_MODE";
	public static readonly string FEATURE_VR_MODE_HIGH_PERFORMANCE = "FEATURE_VR_MODE_HIGH_PERFORMANCE";
	public static readonly string FEATURE_VULKAN_HARDWARE_LEVEL = "FEATURE_VULKAN_HARDWARE_LEVEL";
	public static readonly string FEATURE_VULKAN_HARDWARE_VERSION = "FEATURE_VULKAN_HARDWARE_VERSION";
	public static readonly string FEATURE_WATCH = "FEATURE_WATCH";
	public static readonly string FEATURE_WEBVIEW = "FEATURE_WEBVIEW";
	public static readonly string FEATURE_WIFI = "FEATURE_WIFI";
	public static readonly string FEATURE_WIFI_DIRECT = "FEATURE_WIFI_DIRECT";
	*/
	public static bool IsDebug = false;

	private static AndroidJavaClass unityPlayerClass;
	private static AndroidJavaClass packageManagerClass;
	private static AndroidJavaObject currentActivity;
	private static AndroidJavaObject packageManager;
	private static bool javaClassesLoaded = false;

	private static void loadJavaClasses ()
	{
		javaClassesLoaded = true;
		unityPlayerClass = new AndroidJavaClass ( "com.unity3d.player.UnityPlayer" );
		packageManagerClass = new AndroidJavaClass ( "android.content.pm.PackageManager" );
		currentActivity = unityPlayerClass.GetStatic<AndroidJavaObject> ( "currentActivity" );
		packageManager = currentActivity.Call<AndroidJavaObject> ( "getPackageManager" );
	}

	public static bool HasSystemFeature ( AndroidFeature feature )
	{
		return HasSystemFeature ( feature.ToString () );
	}

	public static bool HasSystemFeature ( string feature )
	{
		if ( Application.platform != RuntimePlatform.Android ) {
			return false;
		}
		if ( !javaClassesLoaded ) {
			loadJavaClasses ();
		}
		int systemFeature = PlayerPrefs.GetInt ( feature, -1 );
		if ( systemFeature == -1 ) {
			string f = packageManagerClass.GetStatic<string> ( feature );
			bool b = packageManager.Call<bool> ( "hasSystemFeature", f );
			PlayerPrefs.SetInt ( feature, b == true ? 1 : 0 );
			if ( IsDebug )
				Debug.Log ( feature + " " + b );
			return b;
		} else {
			if ( IsDebug )
				Debug.Log ( feature + " " + systemFeature );
			return systemFeature == 1 ? true : false;
		}

	}
	/*
	// The logical density of the display
	public static float Density { get; protected set; }

	// The screen density expressed as dots-per-inch
	public static int DensityDPI { get; protected set; }

	// The absolute height of the display in pixels
	public static int HeightPixels { get; protected set; }

	// The absolute width of the display in pixels
	public static int WidthPixels { get; protected set; }

	// A scaling factor for fonts displayed on the display
	public static float ScaledDensity { get; protected set; }

	// The exact physical pixels per inch of the screen in the X dimension
	public static float XDPI { get; protected set; }

	// The exact physical pixels per inch of the screen in the Y dimension
	public static float YDPI { get; protected set; }

	public static void DisplayMetricsAndroid ()
	{
		// Early out if we're not on an Android device
		if ( Application.platform != RuntimePlatform.Android ) {
			return;
		}

		// The following is equivalent to this Java code:
		//
		// metricsInstance = new DisplayMetrics();
		// UnityPlayer.currentActivity.getWindowManager().getDefaultDisplay().getMetrics(metricsInstance);
		//
		// ... which is pretty much equivalent to the code on this page:
		// http://developer.android.com/reference/android/util/DisplayMetrics.html

		using (
			AndroidJavaClass unityPlayerClass = new AndroidJavaClass ( "com.unity3d.player.UnityPlayer" ),
			metricsClass = new AndroidJavaClass ( "android.util.DisplayMetrics" ) ) {
			using (
				AndroidJavaObject metricsInstance = new AndroidJavaObject ( "android.util.DisplayMetrics" ),
				activityInstance = unityPlayerClass.GetStatic<AndroidJavaObject> ( "currentActivity" ),
				windowManagerInstance = activityInstance.Call<AndroidJavaObject> ( "getWindowManager" ),
				displayInstance = windowManagerInstance.Call<AndroidJavaObject> ( "getDefaultDisplay" ) ) {
				displayInstance.Call ( "getMetrics", metricsInstance );
				Density = metricsInstance.Get<float> ( "density" );
				DensityDPI = metricsInstance.Get<int> ( "densityDpi" );
				HeightPixels = metricsInstance.Get<int> ( "heightPixels" );
				WidthPixels = metricsInstance.Get<int> ( "widthPixels" );
				ScaledDensity = metricsInstance.Get<float> ( "scaledDensity" );
				XDPI = metricsInstance.Get<float> ( "xdpi" );
				YDPI = metricsInstance.Get<float> ( "ydpi" );
			}
		}
	}
	*/

}



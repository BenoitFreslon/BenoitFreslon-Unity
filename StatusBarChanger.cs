﻿using UnityEngine;
using System.Collections;

public class StatusBarChanger
{
	#if UNITY_ANDROID
	private static int newStatusBarValue;
	#endif
	public static void Show ()
	{
		#if UNITY_ANDROID
		setStatusBarValue ( 2048 ); // WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN
		#endif
	}

	public static void Hide ()
	{
		#if UNITY_ANDROID
		setStatusBarValue ( 1024 ); // WindowManager.LayoutParams.FLAG_FULLSCREEN; change this to 0 if unsatisfied
		#endif
	}

	private static void setStatusBarValue ( int value )
	{
		#if UNITY_ANDROID
		newStatusBarValue = value;
		using ( var unityPlayer = new AndroidJavaClass ( "com.unity3d.player.UnityPlayer" ) ) {
			using ( var activity = unityPlayer.GetStatic<AndroidJavaObject> ( "currentActivity" ) ) {
				activity.Call ( "runOnUiThread", new AndroidJavaRunnable ( setStatusBarValueInThread ) );
			}
		}
		#endif
	}

	private static void setStatusBarValueInThread ()
	{
		#if UNITY_ANDROID
		using ( var unityPlayer = new AndroidJavaClass ( "com.unity3d.player.UnityPlayer" ) ) {
			using ( var activity = unityPlayer.GetStatic<AndroidJavaObject> ( "currentActivity" ) ) {
				using ( var window = activity.Call<AndroidJavaObject> ( "getWindow" ) ) {
					window.Call ( "setFlags", newStatusBarValue, newStatusBarValue );
				}
			}
		}
		#endif
	}
}
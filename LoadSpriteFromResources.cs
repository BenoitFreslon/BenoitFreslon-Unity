﻿using UnityEngine;
using System.Collections;

public class LoadSpriteFromResources : MonoBehaviour
{
	public string SpriteToLoad = "";
	// Use this for initialization
	void Awake ()
	{
		GetComponent<SpriteRenderer> ().sprite = Resources.Load (SpriteToLoad, typeof(Sprite)) as Sprite;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class CroppedSprite : MonoBehaviour
{
    public Sprite SpriteToCrop;

    void Awake ()
    {
    }
    // Use this for initialization
    void Start ()
    {

    }
#if UNITY_EDITOR
    void OnValidate ()
    {
        Crop = Crop;
    }
#endif
    // Update is called once per frame
    void Update ()
    {
    }

    [SerializeField]
    public Vector2 pivot = new Vector2 ( 0.5f, 0.5f );

    public Vector2 Pivot {
        set {
            pivot = value;
            cropSprite ();
        }
        get {
            return pivot;
        }
    }

    [SerializeField]
    private Vector2 crop = new Vector2 ( 1, 1 );

    public Vector2 Crop {
        set {

            crop = value;

            if ( crop.x > 1f )
                crop.x = 1f;
            if ( crop.y > 1f )
                crop.y = 1f;

            if ( crop.x < 0f )
                crop.x = 0f;
            if ( crop.y < 0f )
                crop.y = 0f;

            cropSprite ();
        }
        get {
            return crop;
        }
    }

    protected void cropSprite ()
    {
        Rect rect = SpriteToCrop.textureRect;
        rect.width *= crop.x;
        rect.height *= crop.y;
        if ( Application.isPlaying )
            GetComponent<SpriteRenderer> ().sprite = Sprite.Create ( SpriteToCrop.texture, rect, Pivot );

    }
}
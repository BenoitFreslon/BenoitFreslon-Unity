#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;


class ProportionalSizeWithTexture
{
	
	[MenuItem("Tools/Benoit/Propotional Scale With Texture")]
	
	static void UpgradeSolutions ()
	{
		// Debug.Log(Selection.activeGameObject);
		if (Selection.activeGameObject) {
			GameObject go = Selection.activeGameObject;
			Debug.Log (go.GetComponent<Renderer>().material.mainTexture.width + " " + go.GetComponent<Renderer>().material.mainTexture.height);
			float scaleX = go.GetComponent<Renderer>().material.mainTexture.width / 100f;
			float scaleZ = go.GetComponent<Renderer>().material.mainTexture.height / 100f;
			go.transform.localScale = new Vector3 (scaleX, 1f, scaleZ);
		}

	}
}

#endif
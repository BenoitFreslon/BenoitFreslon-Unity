using UnityEngine;
using System.Collections;

public class AnimatedTexture : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame

	public Texture2D[] Frames;
	int frame = 0;
	public float FPS = 60f;
	int index = 0;
	public bool IsLooped = true;
	public delegate void AnimationCompleted ();
	public AnimationCompleted OnComplete;

	public bool IsPaused = false;

	void Update () {

		if (IsPaused || Frames.Length == 0) {
			return;
		}
		//Debug.Log(frame % fps);
		int step = Mathf.FloorToInt (1f / FPS * 60f);
		if (step == 0f || frame % step == 0) {

			setFrame (index);

			index++;

			if (index == Frames.Length) {
				index = 0;
				animationCompleted ();
				if (!IsLooped) {
					Pause ();
				}
			}
		}
		frame++;
	}
	void setFrame (int f) {
		if (GetComponent<SpriteRenderer> ()) {
			GetComponent<SpriteRenderer> ().sprite = TextureToSprite (Frames [index]);
		} else {
			GetComponent<Renderer>().material.mainTexture = Frames [index];
		}
	}
	void animationCompleted () {
		//Debug.Log("animationCompleteted: " + name + " " + OnComplete);
		if (OnComplete != null)
			OnComplete ();
	}
	Sprite TextureToSprite (Texture2D t) {
		return Sprite.Create (t, new Rect (0, 0, t.width, t.height), new Vector2 (0.5f, 0.5f));
	}
	public void Play () {
		Reset ();
		Active ();
		Resume ();
	}
	public void Reset () {
		frame = 0;
		index = 0;
		setFrame (index);
	}
	public void Pause () {
		IsPaused = true;
	}
	public void Resume () {
		IsPaused = false;
	}
	public void Active () {
		gameObject.SetActive (true);
	}
	public void Deactive () {
		gameObject.SetActive (false);
		IsPaused = true;
	}
	public float Percentage {
		get {
			return (float)index / (float)Frames.Length;
		}
	}
	void OnDestroy () {
	}
}


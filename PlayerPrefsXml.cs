﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.IO;

public class PlayerPrefXml : MonoBehaviour
{

	public static PlayerPrefXml Ins;
	private List<VariableData> variableData = new List<VariableData>();
	private string path;
	public string fileName = "PlayerData.xml";


	// Use this for initialization
	void Awake()
	{
		Ins = this;
		DontDestroyOnLoad(gameObject);
	}

	// Set
	public void SetFloat(string dataName, float value)
	{
		string stringValue = value.ToString();
		DoSavingProcess(dataName, stringValue, "float");
	}

	public void SetInt(string dataName, int value)
	{
		string stringValue = value.ToString();
		DoSavingProcess(dataName, stringValue, "int");
	}

	public void SetString(string dataName, string value)
	{
		DoSavingProcess(dataName, value, "string");
	}

	// Get
	public float GetFloat(string dataName)
	{

		VariableData vd = new VariableData();
		vd = GetVariableData(dataName, "float");

		float value = 0.0f;
		if (vd.value != "")
			value = float.Parse(vd.value);
		return value;
	}

	public int GetInt(string dataName)
	{
		VariableData vd = new VariableData();
		vd = GetVariableData(dataName, "int");

		int value = 0;
		if (vd.value != "")
			value = int.Parse(vd.value);
		return value;
	}

	public string GetString(string dataName)
	{
		VariableData vd = new VariableData();
		vd = GetVariableData(dataName, "string");

		return vd.value;
	}

	public class VariableData
	{
		public string variableName;
		public string value;
		public string type;
	}

	private void GetFreshList()
	{
		variableData.Clear();
	}

	private bool CheckIfVariableExists(string variableName)
	{
		bool flag = false;
		for (int i = 0; i < variableData.Count; i++)
		{
			if (variableData[i].variableName == variableName)
			{
				flag = true;
			}
		}
		return flag;
	}

	private void SetVariableDataToExistingVariable(string variableName, string value, string type)
	{

		for (int i = 0; i < variableData.Count; i++)
		{
			if (variableData[i].variableName == variableName)
			{
				variableData[i].value = value;
				variableData[i].type = type;
			}
		}
	}

	private void SetVariableDataToNewVariable(string variableName, string value, string type)
	{

		VariableData vbdata = new VariableData();
		vbdata.variableName = variableName;
		vbdata.value = value;
		vbdata.type = type;
		variableData.Add(vbdata);
	}


	private void DoSavingProcess(string variableName, string stringValue, string type)
	{
		GetFreshList();
		Load();

		if (CheckIfVariableExists(variableName))
		{
			SetVariableDataToExistingVariable(variableName, stringValue, type);
			Save();
		}
		else
		{
			SetVariableDataToNewVariable(variableName, stringValue, type);
			Save();
		}
	}

	private VariableData GetVariableData(string variableName, string type)
	{

		GetFreshList();
		Load();

		VariableData vd = new VariableData();
		bool flag = false;
		for (int i = 0; i < variableData.Count; i++)
		{
			if (variableData[i].variableName == variableName)
			{
				flag = true;
				if (type == variableData[i].type)
					vd.value = variableData[i].value;
				else
					vd.value = "";
			}
		}

		if (!flag)
			vd.value = "";

		return vd;
	}




	private void Load()
	{
		if (System.IO.File.Exists(getPath()))
		{
			path = getPath();

			XmlReader reader = XmlReader.Create(path);
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(reader);
			XmlNodeList Data = xmlDoc.GetElementsByTagName("Data");
			for (int i = 0; i < Data.Count; i++)
			{
				// getting data
				XmlNode DataChilds = Data.Item(i);
				// getting all gameObjects stored inside data
				XmlNodeList allvariables = DataChilds.ChildNodes;


				for (int j = 0; j < allvariables.Count; j++)
				{
					XmlNode variable = allvariables.Item(j);

					VariableData vbdata = new VariableData();
					vbdata.variableName = variable.Name;

					XmlNodeList variableValues = variable.ChildNodes;
					//First element have the value stored inside it 
					XmlNode variable_Data1 = variableValues.Item(0);
					vbdata.value = variable_Data1.InnerText;
					//Second element have the type stored inside it 
					XmlNode variable_Data2 = variableValues.Item(1);
					vbdata.type = variable_Data2.InnerText;


					variableData.Add(vbdata);
				}
			}
			reader.Close();
		}
	}


	private void Save()
	{


		path = getPath();
		XmlDocument xmlDoc = new XmlDocument();

		XmlElement elmRoot = xmlDoc.CreateElement("Data");
		xmlDoc.AppendChild(elmRoot);

		for (int i = 0; i < variableData.Count; i++)
		{
			//Creating an xml element with field name
			XmlElement Variable_Object = xmlDoc.CreateElement(variableData[i].variableName);
			//Creating an xml element for saving field value
			XmlElement Variable_Values = xmlDoc.CreateElement("Value");
			Variable_Values.InnerText = variableData[i].value;

			//Creating an xml element for saving field type
			XmlElement Variable_Type = xmlDoc.CreateElement("Type");
			Variable_Type.InnerText = variableData[i].type;

			Variable_Object.AppendChild(Variable_Values);
			Variable_Object.AppendChild(Variable_Type);
			elmRoot.AppendChild(Variable_Object);
		}

		StreamWriter outStream = System.IO.File.CreateText(path);

		xmlDoc.Save(outStream);
		outStream.Close();
	}

	// Following method is used to retrive the relative path as device platform
	private string getPath()
	{
#if UNITY_EDITOR
        return Application.dataPath + "/" + fileName;
#elif UNITY_ANDROID
        return Application.persistentDataPath+fileName;
#elif UNITY_IPHONE
        return Application.persistentDataPath+"/"+fileName;
#elif WINDOWS_UWP
        return Application.persistentDataPath+"/"+fileName;
#else
		return Application.dataPath + "/" + fileName;
#endif
	}
}
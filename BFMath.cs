﻿using UnityEngine;
using System.Collections;

public class BFMath
{

	public static float GetResAngle ( float angle1, float angle2 )
	{
		float a = angle1 - angle2;
		if ( a > 180 ) {
			a = a - 360;
		} else if ( a < -180 ) {
			a = a + 360;
		}
		return a;
	}

	public static float GetAngle ( Vector2 v1, Vector2 v2 )
	{
		return Mathf.Atan2 ( v2.y - v1.y, v2.x - v1.x ) * Mathf.Rad2Deg;
	}

	public static Vector2 RandomVector2 ( float min = -1f, float max = 1f )
	{
		return new Vector2 ( Random.Range ( min, max ), Random.Range ( min, max ) );
	}

	public static Vector3 RandomVector3 ( float min = -1f, float max = 1f )
	{
		return new Vector3 ( Random.Range ( min, max ), Random.Range ( min, max ), Random.Range ( min, max ) );
	}
}

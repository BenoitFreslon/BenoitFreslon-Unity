﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomUIToggle : MonoBehaviour
{
    public Image image;
    public Sprite spriteOn;
    public Sprite spriteOff;

    // Start is called before the first frame update
    void Start ()
    {
        Debug.Log ( name + " " + GetComponent<Toggle> ().isOn );
        IsOn = GetComponent<Toggle> ().isOn;
    }

    // Update is called once per frame
    void Update ()
    {

    }

    public bool IsOn {
        set {
            if ( value ) {
                setOn ();
            } else {
                setOff ();
            }
        }

    }
    void setOn ()
    {
        image.sprite = spriteOn;
    }
    void setOff ()
    {
        image.sprite = spriteOff;
    }
}
